import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/network/api_constant.dart';
import 'package:jualbay/network/api_manager.dart';
import 'package:jualbay/page/produkviewpage.dart';
import 'package:provider/provider.dart';

class PilihanToko extends StatelessWidget {
  const PilihanToko({
    Key key,
    this.imgProduct,
    this.diskonStatus,
    this.codStatus,
    this.judulProduk,
    this.hargaNormal,
    this.hargaDiskon,
    this.jmlDiskon,
    this.displayName = "",
    this.idProduk,
  }) : super(key: key);

  final String imgProduct;
  final bool diskonStatus;
  final bool codStatus;
  final String judulProduk;
  final int hargaNormal;
  final int hargaDiskon;
  final dynamic jmlDiskon;
  final String displayName;
  final int idProduk;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 95,
      height: 160,
      margin: EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.3),
            spreadRadius: 0.1,
            blurRadius: 2,
            offset: Offset(0, 1), // changes position of shadow
          ),
        ],
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        children: [
          InkWell(
            onTap: () {
              final DetailProdukProvider detailProdukProvider =
                  Provider.of<DetailProdukProvider>(context, listen: false);
              detailProdukProvider.idDetailProduk = idProduk.toString();
              detailProdukProvider.quantity = 0;
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ProdukView(
                            // idDetailProduk: idProduk.toString(),
                          )));
            },
            child: Container(
              height: 100,
              child: Stack(
                children: [
                  CachedNetworkImage(
                    // imageUrl:  imgProduct,
                    imageUrl: ApiConstant.baseUrl +
                        "uploads/product_image/product_68222139_1_thumb.jpeg",
                    imageBuilder: (context, imageProvider) => Container(
                      width: 120,
                      height: 120,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10)),
                        image: DecorationImage(
                            image: imageProvider, fit: BoxFit.cover),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(5.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: Container(
                      margin: EdgeInsets.only(bottom: 3),
                      child: Text(
                        judulProduk,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    // margin: EdgeInsets.symmetric(vertical: 3),
                    child: Text(
                      "Rp. " + hargaDiskon.toString(),
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontSize: 14,
                          color: clrUnguDark,
                          fontWeight: FontWeight.w800),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
