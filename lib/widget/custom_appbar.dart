// <<<<<<< HEAD 
import 'package:flutter/material.dart';
import 'package:jualbay/helpers/colors.dart';

class CustomAppbar extends StatefulWidget {
  final Color backgroundColor;
  final Color preIconColor, postIconColor;
  final Widget title;
  final List<Widget> actions;
  final Widget leading;
  const CustomAppbar(
      {Key key,
      this.backgroundColor,
      this.title,
      this.preIconColor,
      this.postIconColor,
      this.actions,
      this.leading})
      : super(key: key);
  @override
  CustomAppbarState createState() => CustomAppbarState();
}

class CustomAppbarState extends State<CustomAppbar> {
  double _opacity = 0;
  Color get objectColor =>
      _opacity < 1 ? widget.preIconColor : widget.postIconColor;
  Color get objectColor2 =>
      _opacity < 1 ? widget.preIconColor : widget.postIconColor;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      flexibleSpace: SafeArea(
        child: Row(
          children: [
            Expanded(
              child: Container(
                  height: 40,
                  decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.2),
                        spreadRadius: 0.5,
                        blurRadius: 5,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(50)),
                    color: _opacity < 1 ? Colors.white : Colors.grey[50],
                  ),
                  // padding: EdgeInsets.fromLTRB(0, 3, 5, 8),
                  margin: EdgeInsets.fromLTRB(20, 10, 5, 10),
                  child: TextFormField(
                    cursorColor: Colors.black,
                    style: TextStyle(
                      color: _opacity < 1 ? Colors.black : Colors.black,
                    ),
                    decoration: new InputDecoration(
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      prefixIcon: Icon(
                        Icons.search,
                        size: 20,
                        color: _opacity < 1 ? Colors.grey[700] : clrSubText,
                      ),
                      contentPadding: EdgeInsets.only(
                          left: 5, bottom: 19, top: 0, right: 5),
                      // hintText: 'sLabel',
                      // hintStyle: TextStyle(
                      //   color: _opacity < 1 ? Colors.grey[700] : Colors.white,
                      // ),
                    ),
                  )),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(0, 5, 10, 10),
              child: IconButton(
                // splashRadius: 30,
                iconSize: 25,
                splashRadius: 20,
                onPressed: () {},
                icon: Icon(
                  Icons.notifications,
                  color: objectColor,
                ),
              ),
            ),
          ],
        ),
      ),
      // title: TextField(
      //   decoration: InputDecoration(
      //     isDense: true,
      //     prefixIcon: Icon(
      //       Icons.search,
      //       color: objectColor,
      //     ),
      //     border: OutlineInputBorder(
      //       borderSide: new BorderSide(color: Colors.yellowAccent),
      //       borderRadius: BorderRadius.all(Radius.circular(50)),
      //     ),
      //   ),
      // ),
      elevation: 0,
      iconTheme: IconThemeData(
        color: objectColor,
      ),
      actions: widget.actions,
      leading: widget.leading,
      textTheme: ThemeData.light().textTheme.copyWith(
          headline6: TextStyle(
              color: objectColor, fontWeight: FontWeight.bold, fontSize: 20)),
      backgroundColor: widget.backgroundColor != null
          ? widget.backgroundColor.withOpacity(_opacity)
          : Colors.white.withOpacity(_opacity),
    );
  }

  void setOpacity(double value) {
    setState(() {
      _opacity = value;
    });
  }
}
// =======
// import 'package:flutter/material.dart';
// import 'package:jualbay/helpers/colors.dart';

// class CustomAppbar extends StatefulWidget {
//   final Color backgroundColor;
//   final Color preIconColor, postIconColor;
//   final Widget title;
//   final List<Widget> actions;
//   final Widget leading;
//   const CustomAppbar(
//       {Key key,
//       this.backgroundColor,
//       this.title,
//       this.preIconColor,
//       this.postIconColor,
//       this.actions,
//       this.leading})
//       : super(key: key);
//   @override
//   CustomAppbarState createState() => CustomAppbarState();
// }

// class CustomAppbarState extends State<CustomAppbar> {
//   double _opacity = 0;
//   Color get objectColor =>
//       _opacity < 1 ? widget.preIconColor : widget.postIconColor;
//   Color get objectColor2 =>
//       _opacity < 1 ? widget.preIconColor : widget.postIconColor;

//   @override
//   Widget build(BuildContext context) {
//     return AppBar(
//       automaticallyImplyLeading: false,
//       flexibleSpace: SafeArea(
//         child: Row(
//           children: [
//             Expanded(
//               child: Container(
//                   height: 40,
//                   decoration: BoxDecoration(
//                     boxShadow: [
//                       BoxShadow(
//                         color: Colors.black.withOpacity(0.2),
//                         spreadRadius: 0.5,
//                         blurRadius: 5,
//                         offset: Offset(0, 1), // changes position of shadow
//                       ),
//                     ],
//                     borderRadius: BorderRadius.all(Radius.circular(50)),
//                     color: _opacity < 1 ? Colors.white : Colors.grey[50],
//                   ),
//                   // padding: EdgeInsets.fromLTRB(0, 3, 5, 8),
//                   margin: EdgeInsets.fromLTRB(20, 10, 5, 10),
//                   child: TextFormField(
//                     cursorColor: Colors.black,
//                     style: TextStyle(
//                       color: _opacity < 1 ? Colors.black : Colors.black,
//                     ),
//                     decoration: new InputDecoration(
//                       border: InputBorder.none,
//                       focusedBorder: InputBorder.none,
//                       enabledBorder: InputBorder.none,
//                       errorBorder: InputBorder.none,
//                       disabledBorder: InputBorder.none,
//                       prefixIcon: Icon(
//                         Icons.search,
//                         size: 20,
//                         color: _opacity < 1 ? Colors.grey[700] : clrSubText,
//                       ),
//                       contentPadding: EdgeInsets.only(
//                           left: 5, bottom: 19, top: 0, right: 5),
//                       // hintText: 'sLabel',
//                       // hintStyle: TextStyle(
//                       //   color: _opacity < 1 ? Colors.grey[700] : Colors.white,
//                       // ),
//                     ),
//                   )),
//             ),
//             Container(
//               margin: EdgeInsets.fromLTRB(0, 5, 10, 10),
//               child: IconButton(
//                 // splashRadius: 30,
//                 iconSize: 25,
//                 splashRadius: 20,
//                 onPressed: () {},
//                 icon: Icon(
//                   Icons.notifications,
//                   color: objectColor,
//                 ),
//               ),
//             ),
//           ],
//         ),
//       ),
//       // title: TextField(
//       //   decoration: InputDecoration(
//       //     isDense: true,
//       //     prefixIcon: Icon(
//       //       Icons.search,
//       //       color: objectColor,
//       //     ),
//       //     border: OutlineInputBorder(
//       //       borderSide: new BorderSide(color: Colors.yellowAccent),
//       //       borderRadius: BorderRadius.all(Radius.circular(50)),
//       //     ),
//       //   ),
//       // ),
//       elevation: 0,
//       iconTheme: IconThemeData(
//         color: objectColor,
//       ),
//       actions: widget.actions,
//       leading: widget.leading,
//       textTheme: ThemeData.light().textTheme.copyWith(
//           headline6: TextStyle(
//               color: objectColor, fontWeight: FontWeight.bold, fontSize: 20)),
//       backgroundColor: widget.backgroundColor != null
//           ? widget.backgroundColor.withOpacity(_opacity)
//           : Colors.white.withOpacity(_opacity),
//     );
//   }

//   void setOpacity(double value) {
//     setState(() {
//       _opacity = value;
//     });
//   }
// }
// >>>>>>> 625fb91f5671b4fae18b820fec00ec47d18cf23d
