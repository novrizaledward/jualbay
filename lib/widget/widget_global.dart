// <<<<<<< HEAD
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/network/api_constant.dart';
import 'package:jualbay/network/api_manager.dart';
import 'package:jualbay/page/produkviewpage.dart';
import 'package:provider/provider.dart';


// ignore: must_be_immutable
class LoadingWidget extends StatelessWidget {
  double size, margin;

  LoadingWidget({this.size = 24, this.margin = 12});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: size,
      margin: EdgeInsets.all(
        margin,
      ),
      alignment: Alignment.center,
      child: Container(
        height: size,
        width: size,
        child: CircularProgressIndicator(),
      ),
    );
  }
}

class WgtProdukView extends StatelessWidget {
  const WgtProdukView({
    Key key,
    this.imgProduct = "",
    this.diskonStatus ,
    this.codStatus , 
    this.namaProduk = "",
    this.hargaNormal ,
    this.hargaDiskon  ,
    this.jmlDiskon = "", 
    this.displayName = "",
    this.idProduk,
    
    //  imgSlider,
  }) : super(key: key);

  final String imgProduct;
  final bool diskonStatus;
  final bool codStatus;
  final String namaProduk;
  final int hargaNormal;
  final int hargaDiskon;
  // final int hargaBiasa;
  // final int hargaKurang;
  final dynamic jmlDiskon;
  final String displayName;
  final int idProduk;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 95,
      height: 165,
      margin: EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.3),
            spreadRadius: 0.1,
            blurRadius: 2,
            offset: Offset(0, 1), // changes position of shadow
          ),
        ],
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: InkWell(
        onTap: () {
           final DetailProdukProvider detailProdukProvider =
                    Provider.of<DetailProdukProvider>(context, listen: false);

                detailProdukProvider.idDetailProduk = idProduk.toString();
                detailProdukProvider.quantity = 0;
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ProdukView(
                              // idDetailProduk: idProduk.toString(),
                            )));
        },
              child: Column(
          children: [
            Container(
              height: 95,
              child: Stack(
                children: [
                  CachedNetworkImage(
                    imageUrl:  imgProduct,
                    imageBuilder: (context, imageProvider) => Container(
                      width: 120,
                      height: 120,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10)),
                        image: DecorationImage(
                            image: imageProvider, fit: BoxFit.cover),
                      ),
                    ),
                  ),
                  diskonStatus
                      ? Positioned(
                          top: 0,
                          right: 0,
                          child: Container(
                            height: 20,
                            width: 35,
                            decoration: BoxDecoration(
                              color: clrUnguDark,
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(10),
                                  topRight: Radius.circular(10)),
                            ),
                            child: Center(
                              child: Text(
                                jmlDiskon.toString(),
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        )
                      : Container(),
                  codStatus
                      ? Positioned(
                          bottom: 0,
                          left: 0,
                          child: SvgPicture.asset("assets/svg/icon-cod.svg"),
                        )
                      : Container()
                ],
              ),
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Container(
                        margin: EdgeInsets.only(bottom: 3),
                        child: Text(
                          namaProduk,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 2,
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      // margin: EdgeInsets.symmetric(vertical: 3),
                      child: Text(
                       "Rp. " + hargaNormal.toString(),
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 12,
                            color: clrUnguDark,
                            fontWeight: FontWeight.w800),
                      ),
                    ),
                    Container(
                      child: Text(
                       "Rp. " + hargaDiskon.toString(),
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        style: TextStyle(
                            fontSize: 12,
                            decoration: TextDecoration.lineThrough,
                            color: clrSubText,
                            fontWeight: FontWeight.w800),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(vertical: 3),
                      child: Text(
                        displayName,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 7,
                            color: clrUnguDark,
                            fontWeight: FontWeight.w800),
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class CostumWgtList extends StatelessWidget {
  const CostumWgtList({
    Key key,
    this.icon,
    this.title,
    this.onTap,
    this.svg,
    this.svgUse = false,
    this.iconUse,
  }) : super(key: key);

  final String svg;
  final String title;
  final VoidCallback onTap;
  final IconData icon;
  final bool svgUse;
  final bool iconUse;
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 5),
        child: Row(
          children: [
            svgUse == true
                ? SvgPicture.asset(svg)
                : Icon(
                    icon,
                    color: clrIcon,
                  ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          title,
                          style: TextStyle(
                              fontWeight: FontWeight.w700, fontSize: 15),
                        ),
                        IconButton(
                          onPressed: onTap,
                          icon: Icon(
                            Icons.arrow_forward_ios,
                            size: 15,
                            color: clrSubText,
                          ),
                        )
                      ],
                    ),
                    Divider(
                      height: 0,
                      endIndent: 20,
                      thickness: 2,
                    )
                  ],
                ),
              ),
            )
          ],
        ));
  }
}

class AvatarPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = Colors.white
      ..style = PaintingStyle.fill;

    Path path = Path()..moveTo(0, 20);
  }

  @override
  bool shouldRepaint(AvatarPainter oldDelegate) => false;

  @override
  bool shouldRebuildSemantics(AvatarPainter oldDelegate) => false;
}

class TitleOfSection extends StatelessWidget {
  const TitleOfSection({
    Key key,
    this.title,
    this.insertRow = false,
    this.action = "",
    this.ontap,
  }) : super(key: key);

  final String title;
  final bool insertRow;
  final String action;
  final VoidCallback ontap;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        insertRow
            ? Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    title,
                    style: TextStyle(
                      fontWeight: FontWeight.w900,
                      color: Colors.black,
                      fontSize: 16,
                    ),
                  ),
                  SizedBox(width: 5),
                  Icon(
                    Icons.star,
                    color: Colors.amber,
                    size: 15,
                  ),
                  SizedBox(width: 5),
                  Text(
                    "4.5",
                    style: TextStyle(
                      fontWeight: FontWeight.w900,
                      color: Colors.black,
                      fontSize: 16,
                    ),
                  ),
                  SizedBox(width: 5),
                  Text(
                    "(37 Ulasan)",
                    style: TextStyle(
                      fontWeight: FontWeight.w100,
                      color: Colors.black,
                      fontSize: 16,
                    ),
                  ),
                ],
              )
            : Text(
                title,
                style: TextStyle(
                  fontWeight: FontWeight.w900,
                  color: Colors.black,
                  fontSize: 16,
                ),
              ),
        InkWell(onTap: ontap, child: Text(action))
      ],
    );
  }
}


class WgtProdukGrid extends StatelessWidget {
  const WgtProdukGrid({
    Key key,
    this.imgProduct = "",
    this.diskonStatus ,
    this.codStatus , 
    this.namaProduk = "",
    this.hargaNormal ,
    this.hargaDiskon  ,
    this.jmlDiskon = "", 
    this.displayName = "",
    
    //  imgSlider,
  }) : super(key: key);

  final String imgProduct;
  final bool diskonStatus;
  final bool codStatus;
  final String namaProduk;
  final int hargaNormal;
  final int hargaDiskon;
  // final int hargaBiasa;
  // final int hargaKurang;
  final dynamic jmlDiskon;
  final String displayName;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 60,
      height: 100,
      margin: EdgeInsets.symmetric(horizontal: 5),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.3),
            spreadRadius: 0.1,
            blurRadius: 2,
            offset: Offset(0, 1), // changes position of shadow
          ),
        ],
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Column(
        children: [
          Container(
            height: 65,
            child: Stack(
              children: [
                Center(
                  child: CachedNetworkImage(
                    imageUrl: ApiConstant.baseUrl + imgProduct,
                    imageBuilder: (context, imageProvider) => Container(
                      width: 65,
                      height: 65,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10)),
                        image: DecorationImage(
                            image: imageProvider, fit: BoxFit.cover),
                      ),
                    ),
                  ),
                ),
                diskonStatus
                    ? Positioned(
                        top: 0,
                        right: 0,
                        child: Container(
                          height: 15,
                          width: 25,
                          decoration: BoxDecoration(
                            color: clrUnguDark,
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(10),
                                topRight: Radius.circular(10)),
                          ),
                          child: Center(
                            child: Text(
                              jmlDiskon,
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      )
                    : Container(),
                codStatus
                    ? Positioned(
                        bottom: 0,
                        left: 0,
                        child: SvgPicture.asset("assets/svg/icon-cod.svg"),
                      )
                    : Container()
              ],
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(1.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    
                      child: Container(
                        margin: EdgeInsets.only(bottom: 2),
                        child: Center(
                          child: Text(
                            namaProduk,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                            style: TextStyle(
                              fontSize: 9,
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),
                    ),
                  
                    Container(
                      // margin: EdgeInsets.symmetric(vertical: 3),
                      padding: EdgeInsets.only(left: 1),
                      child: Center(
                        child: Text(
                         "Rp. " + hargaNormal.toString(),
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.left,
                          style: TextStyle(
                              fontSize: 9,
                              color: clrUnguDark,
                              fontWeight: FontWeight.w800),
                        ),
                      ),
                    ),
                  
                  Container(
                      child: Center(
                        child: Text(
                         "Rp. " + hargaDiskon.toString(),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: TextStyle(
                              fontSize: 9,
                              decoration: TextDecoration.lineThrough,
                              color: clrSubText,
                              fontWeight: FontWeight.w800),
                        ),
                      ),
                    ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class ExpandableContainer extends StatelessWidget {
  final bool expanded;
  final double collapsedHeight;
  final double expandedHeight;
  final Widget child;

  ExpandableContainer({
    @required this.child,
    this.collapsedHeight = 0.0,
    this.expandedHeight = 300.0,
    this.expanded = true,
  });

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    return AnimatedContainer(
      duration: Duration(milliseconds: 500),
      curve: Curves.easeInOut,
      width: screenWidth,
      height: expanded ? expandedHeight : collapsedHeight,
      child: Container(
        child: child,
      ),
    );
  }
}

class CustomSlider extends StatelessWidget {
  final double percentage;
  final double width;
  CustomSlider({
    this.percentage,
    this.width,
  });
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: double.infinity,
          height: 7,
          decoration: BoxDecoration(
            color: clrButtonbg,
            borderRadius: BorderRadius.circular(10),
          ),
        ),
        Container(
          width: percentage == null ? 0 : width * percentage,
          // width: double.infinity,
          height: 7,
          decoration: BoxDecoration(
            color: clrRating,
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ],
    );
  }
}


class PrimaryButton extends StatelessWidget {
  const PrimaryButton(
      {Key key,
      this.onPress,
      this.title,
      this.color,
      this.width,
      this.marginhorizontal,
      this.titleColor,
      this.height,
      this.borderColor = Colors.red})
      : super(key: key);

  final VoidCallback onPress;
  final String title;
  final Color color;
  final double width;
  final double marginhorizontal;
  final Color titleColor;
  final double height;
  final Color borderColor;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height ?? 40,
      width: width,
      margin: EdgeInsets.symmetric(horizontal: marginhorizontal ?? 0),
      child: FlatButton(
        onPressed: onPress,
        child: Text(
          title,
          style: TextStyle(color: titleColor ?? Colors.white, fontSize: 18),
        ),
        color: color,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(7),
            side: BorderSide(color: borderColor ?? Colors.red)),
      ),
    );
  }
}

class MySeparator extends StatelessWidget {
  final double height;
  final Color color;

  const MySeparator({this.height = 1, this.color = Colors.black});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final boxWidth = constraints.constrainWidth();
        final dashWidth = 3.0;
        final dashHeight = height;
        final dashCount = (boxWidth / (2 * dashWidth)).floor();
        return Flex(
          children: List.generate(dashCount, (_) {
            return SizedBox(
              width: dashWidth,
              height: dashHeight,
              child: DecoratedBox(
                decoration: BoxDecoration(color: color),
              ),
            );
          }),
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          direction: Axis.horizontal,
        );
      },
    );
  }
}
