import 'dart:convert';

import 'package:dio/dio.dart' as htttp_dio;
import 'package:jualbay/model/postModel.dart';

class ApiProvider {
  htttp_dio.Dio dio = htttp_dio.Dio();

  Future<PostModel> getDataPostFromApiAsycn() async {
    htttp_dio.Response response;

    response = await dio.get(
        "https://api.jualbuy.com/promoday?signature=50df4ebf210f109d95da453aa4500fbce018ed8c");
    final rawData = PostModel.fromJson(response.data);

    // print(response.data);
    print(response.statusCode);
    print(response.statusMessage);
    return rawData;
  }
}
