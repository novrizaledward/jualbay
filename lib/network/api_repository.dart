import 'package:jualbay/model/postModel.dart';
import 'package:jualbay/network/api_provider.dart';

class ApiRepository {
  ApiProvider _provider = ApiProvider();
  Future<PostModel> get getDataPostFromApi =>
      _provider.getDataPostFromApiAsycn();
}
