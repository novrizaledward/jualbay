// <<<<<<< HEAD
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:jualbay/model/detailprodukmodel.dart';
import 'package:jualbay/model/model_banner.dart';
import 'package:jualbay/model/model_flashsale.dart';
import 'package:jualbay/model/model_product_view.dart';
import 'package:jualbay/model/model_rekom_produk.dart';
import 'package:jualbay/model/model_rekom_scroll.dart';
import 'package:jualbay/model/model_umkm.dart';
import 'package:jualbay/model/model_slider.dart';
import 'package:jualbay/model/model_promo.dart';
import 'package:jualbay/model/model_shortcut.dart';
// import 'package:jualbay/model/modelrekomkategori.dart'; 


class UmkmManager extends ChangeNotifier {
  ModelUmkm _modelUmkm;
  ModelUmkm get modelUmkm => _modelUmkm;

  Future<ModelUmkm> fetchUmkm() async {
    final respone = await http.get(
        "https://api.jualbuy.com/umkm?signature=9d88b9de20f586840998c6743b337ff2d5147850");
    var result = modelUmkmFromJson(respone.body);
    _modelUmkm = result;
    notifyListeners();
    return result;
  }
}

class SliderManager extends ChangeNotifier {
  ModelSlider _modelSlider;
  ModelSlider get modelSlider => _modelSlider;

  Future<ModelSlider> fetchSlider() async {
    final respone = await http.get(
        "https://api.jualbuy.com/slider?signature=40acf2c7387ea1caf898fa83ae5929eecaf0c6f2&position=h1");
    var result = modelSliderFromJson(respone.body);
    _modelSlider = result;
    notifyListeners();
    return result;
  }
}

 class PromoManager extends ChangeNotifier {
  ModelPromo _modelPromo;
  ModelPromo get modelPromo => _modelPromo;

  Future<ModelPromo> fetchPromo() async {
    final respone = await http.get(
        "https://api.jualbuy.com/promoday?signature=50df4ebf210f109d95da453aa4500fbce018ed8c");
    var result = modelPromoFromJson(respone.body);
    _modelPromo = result;
    notifyListeners();
    return result;
  }
}

 class ListManager extends ChangeNotifier {
  ModelShortcut _modelShortcut;
  ModelShortcut get modelShortcut => _modelShortcut;

  Future<ModelShortcut> fetchShortcut() async {
    final respone = await http.get(
        "https://api.jualbuy.com/shortcut?signature=475a53e870bab6e88b660bb38bf50c90b71abeca");
    var result = modelShortcutFromJson(respone.body);
    _modelShortcut = result;
    notifyListeners();
    return result;
  }
}

 class IklanManager extends ChangeNotifier {
  ModelBanner _modelBanner;
  ModelBanner get modelBanner => _modelBanner;

  Future<ModelBanner> fetchBanner() async {
   
    final respone = await http.get(
        // "https://api.jualbuy.com/app/banner",body: data);
        "https://api.jualbuy.com/banner?signature=c5484a0d7c8b79dba607af00f76b7f1cfd9ddff1");
    var result = modelBannerFromJson(respone.body); 
    _modelBanner = result;
    notifyListeners();
    return result;
  }
}

class ApiManager extends ChangeNotifier {
  ModelFlashSale _modelFlashSale;
  ModelFlashSale get modelFlashSale => _modelFlashSale;

  Future<ModelFlashSale> fetchFlashSale() async {
    final respone = await http.get(
        "https://api.jualbuy.com/flashsale?signature=7bb595fe434605a796b969c3f0391d49095039f4");
    var result = modelFlashSaleFromJson(respone.body);
    _modelFlashSale = result;
    notifyListeners();
    return result;
  }
}



class RekomManager extends ChangeNotifier {
  ModelRekomProduk _modelRekomProduk;
  ModelRekomProduk get modelRekomProduk => _modelRekomProduk;

  Future<ModelRekomProduk> fetchRekomProduk() async {
    final respone = await http.get(
        "https://api.jualbuy.com/rekom?signature=76307004d1a776aea2bb9057d50e960f789bf54c");
    var result = modelRekomProdukFromJson(respone.body);
    _modelRekomProduk = result;
    notifyListeners();
    return result;
  }
}

class RekomScrollManager extends ChangeNotifier {
  ModelRekomScroll _modelRekomScroll;
  ModelRekomScroll get modelRekomScroll => _modelRekomScroll;

  Future<ModelRekomScroll> fetchRekomScroll() async {
    final respone = await http.post(
        "https://api.jualbuy.com/rekom?signature=76307004d1a776aea2bb9057d50e960f789bf54c&request_type=scroll&page=2");
    var result = modelRekomScrollFromJson(respone.body);
    _modelRekomScroll = result;
    notifyListeners();
    return result;
  }
}



class ProductViewManager extends ChangeNotifier {
  ModelProductView _modelProductView;
  ModelProductView get modelProductView => _modelProductView;

  String _id = "0";
  String get idDetailProduct => _id;
    set idDetailProduct(String value){
      _id = value;
      notifyListeners();
    }

  int _quantity = 0;
  int get quantity => _quantity;
    set quantity(int value) {
      _quantity = value;
      notifyListeners();
    }

  Future<ModelProductView> fetchModelProductView() async {
    final respone = await http.get(
        "https://api.jualbuy.com/productview?signature=96e77ad4947f6e7af76f200b2ebb1bfd37eba950&product_id=" + _id);
    var result = modelProductViewFromJson(respone.body);
    _modelProductView = result;
    notifyListeners();
    return result;
  }
}

class DetailProdukProvider extends ChangeNotifier {
  DetailProdukModel _detailProdukModel;
  DetailProdukModel get detailProdukModel => _detailProdukModel;

  String _id = "0";
  String get idDetailProduk => _id;
  set idDetailProduk(String value) {
    _id = value;
    notifyListeners();
  }

  int _quantity = 0;
  int get quantity => _quantity;

  set quantity(int value) {
    _quantity = value;
    notifyListeners();
  }

  Future<DetailProdukModel> fetchDetailProduk() async {
    final response = await http.get(
        'https://api.jualbuy.com/productview?signature=96e77ad4947f6e7af76f200b2ebb1bfd37eba950&product_id='+_id);
    var jsonData = detailProdukModelFromJson(response.body);
    notifyListeners();
    _detailProdukModel = jsonData;
    return jsonData;
  }
}

// class RekomProdukKategoriProvider extends ChangeNotifier {
//   ModelRekomKategori _modelRekomKategori;
//   ModelRekomKategori get modelRekomKategori => _modelRekomKategori;

//   String _id = "0";
//   String get idDetailProduk => _id;
//   set idDetailProduk(String value) {
//     _id = value;
//     notifyListeners();
//   }

//   int _quantity = 0;
//   int get quantity => _quantity;

//   set quantity(int value) {
//     _quantity = value;
//     notifyListeners();
//   }

//   Future <ModelRekomKategori> fetchRekomKategori() async {
//     final response = await http.get(
//         'https://api.jualbuy.com/rekom?signature=76307004d1a776aea2bb9057d50e960f789bf54c&id_category='+_id);
//     var hasil = modelRekomKategoriFromJson(response.body);
//     notifyListeners();
//     _modelRekomKategori = hasil;
//     return hasil;
//   }
// }


 