var kurir = [
  {
    "nama": "Jne",
    "type": [
      {
        "nama_type": "REG",
        "ket_type": "estimasi 5-6 Hari",
        "harga": 25000,
      },
      {
        "nama_type": "YES",
        "ket_type": "Barang akan dikirim besok",
        "harga": 35000,
      },
      {
        "nama_type": "Same Day",
        "ket_type": "Barang akan diterima dihari yang sama",
        "harga": 50000,
      }
    ]
  },
  {
    "nama": "JNT Express",
    "type": [
      {
        "nama_type": "REG",
        "ket_type": "estimasi 5-6 Hari",
        "harga": 25000,
      },
      {
        "nama_type": "YES",
        "ket_type": "Barang akan dikirim besok",
        "harga": 35000,
      },
      {
        "nama_type": "Same Day",
        "ket_type": "Barang akan diterima dihari yang sama",
        "harga": 50000,
      }
    ]
  },
  {
    "nama": "SiCepat",
    "type": [
      {
        "nama_type": "REG",
        "ket_type": "estimasi 5-6 Hari",
        "harga": 25000,
      },
      {
        "nama_type": "YES",
        "ket_type": "Barang akan dikirim besok",
        "harga": 35000,
      },
      {
        "nama_type": "Same Day",
        "ket_type": "Barang akan diterima dihari yang sama",
        "harga": 50000,
      }
    ]
  },
  {
    "nama": "Gojek",
    "type": [
      {
        "nama_type": "REG",
        "ket_type": "estimasi 5-6 Hari",
        "harga": 25000,
      },
      {
        "nama_type": "YES",
        "ket_type": "Barang akan dikirim besok",
        "harga": 35000,
      },
      {
        "nama_type": "Same Day",
        "ket_type": "Barang akan diterima dihari yang sama",
        "harga": 50000,
      }
    ]
  }
];
