class Pengiriman {
  final String kurir;
  final String estimasi;
  final String type;
  final String ket;
  final int harga;

  Pengiriman({this.type, this.ket, this.harga, this.kurir, this.estimasi});
}

List<Pengiriman> dummypengiriman = [
  Pengiriman(
    kurir: 'JNE',
    estimasi: '2-3 hari',
    type: 'REG',
    ket: 'barang akan diriki',
    harga: 15000,
  ),
  Pengiriman(
    kurir: 'JNE Express',
    estimasi: '2-3 hari',
    type: 'ref',
    ket: 'barang akan diriki',
    harga: 15000,
  ),
  Pengiriman(
    kurir: 'SiCepat',
    estimasi: '2-3 hari',
    type: 'gge',
    ket: 'barang akan diriki',
    harga: 15000,
  ),
  Pengiriman(
    kurir: 'GoJek',
    estimasi: '2-3 hari',
    type: 'llk',
    ket: 'barang akan diriki',
    harga: 15000,
  ),
];
