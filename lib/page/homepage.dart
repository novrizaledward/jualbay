// <<<<<<< HEAD
import 'package:flutter/material.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/widget/custom_appbar.dart';

import 'component/component_homepage/custom_banner_home.dart';
import 'component/component_homepage/custom_flash_sale.dart';
import 'component/component_homepage/custom_ikm_umkm.dart';
import 'component/component_homepage/custom_kategori_list.dart';
import 'component/component_homepage/custom_produk_ukm.dart';
import 'component/component_homepage/custom_promo_hari_ini.dart';
// import 'component/component_homepage/testing_promo.dart';
import 'component/component_homepage/custom_rekomendasisection.dart';
import 'component/component_homepage/custom_slider.dart';
import 'component/component_homepage/custom_slider_ads.dart';
import 'component/component_homepage/info_saldo.dart';

class HomePage extends StatefulWidget {
  HomePage({
    Key key,
  }) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final ScrollController scrollController = ScrollController();
  double opacity = 0;
  final GlobalKey<CustomAppbarState> appBarKey = GlobalKey();

  List<Tab> tabList = List();
  @override
  void initState() {
    scrollController.addListener(() {
      double temp = (1 / 200) * (scrollController.position.pixels);
      if (temp < 1)
        appBarKey.currentState.setOpacity(temp);
      else
        appBarKey.currentState.setOpacity(1);
    });

    super.initState();
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 5,
      child: Scaffold(
        backgroundColor: clrbg,
        extendBodyBehindAppBar: true,
        appBar: PreferredSize(
            child: CustomAppbar(
              key: appBarKey,
              postIconColor: clrSubText,
              preIconColor: Colors.white,
            ),
            preferredSize: Size(MediaQuery.of(context).size.width, 50)),
        body: SingleChildScrollView(
          controller: scrollController,
          child: Column(
            children: [
              Stack(
                children: [
                  CustomSliderHome(),
                  Column(
                    children: [
                      InfoSaldo(),
                      KategoriList(),
                      BannerHome(),
                      CustomFlashSale(),
                      SliderAds(),
                      PromoHariIni(),
                      // PromoDay(),
                      IkmdanUmkm(),
                      ProdukUkm(),
                      RekomendasiSection(),
                      // RekomendasiProduk(),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
