// <<<<<<< HEAD
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/page/component/component_homepage/info_saldo.dart';

import 'component/component_profilpage/custom_menuprofilakun.dart';
import 'component/component_profilpage/custom_menuprofilbuttonkeluar.dart';
import 'component/component_profilpage/custom_menuprofilicon.dart';
import 'component/component_profilpage/custom_menuprofilinfolainnya.dart';
import 'component/component_profilpage/custom_menuprofilpesanan.dart';

class ProfilPage extends StatelessWidget {
  const ProfilPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   elevation: 0,
      //   backgroundColor: Colors.transparent,
      //   leading: SvgPicture.asset(
      //     "assets/svg/profilpage/faq.svg",
      //     width: 24,
      //     height: 24,
      //     fit: BoxFit.scaleDown,
      //   ),
      //   actions: [
      //     Container(
      //       margin: EdgeInsets.symmetric(horizontal: 10),
      //       child: SvgPicture.asset(
      //         "assets/svg/profilpage/settings.svg",
      //         width: 24,
      //         height: 24,
      //         fit: BoxFit.scaleDown,
      //       ),
      //     ),
      //   ],
      // ),
      body: Stack(
        children: [
          Container(
            height: 300,
            decoration: BoxDecoration(
                gradient: LinearGradient(
              colors: [
                clrUnguDark,
                clrUnguLight,
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              tileMode: TileMode.mirror,
            )),
          ),
          SingleChildScrollView(
            child: Stack(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(top: 150),
                  decoration: BoxDecoration(
                    color: clrbg,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(50),
                        topRight: Radius.circular(50)),
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 60,
                      ),
                      Text(
                        "Kucing Aer",
                        style: TextStyle(
                          fontSize: 25,
                          fontWeight: FontWeight.w900,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Welcome to the Member Center",
                        style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      InfoSaldo(
                        marginTop: 20,
                      ),
                      MenuProfilIcon(),
                      MenuProfilPesanan(),
                      MenuProfilAkun(),
                      MenuProfilInfoLainnya(),
                      MenuProfilButtonKeluar(),
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 20),
                        child: Image.asset("assets/img/logo.png"),
                      ),
                    ],
                  ),
                ),
                SafeArea(
                  child: Container(
                    height: 170,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              SvgPicture.asset(
                                "assets/svg/profilpage/faq.svg",
                                width: 24,
                                height: 24,
                                fit: BoxFit.scaleDown,
                              ),
                              Container(
                                child: SvgPicture.asset(
                                  "assets/svg/profilpage/settings.svg",
                                  width: 25,
                                  height: 25,
                                  fit: BoxFit.scaleDown,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.black,
                            shape: BoxShape.circle,
                          ),
                          child: CircleAvatar(
                            backgroundColor: clrMerahDark,
                            radius: 50,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
