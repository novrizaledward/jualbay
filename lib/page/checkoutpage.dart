import 'package:flutter/material.dart';
import 'package:jualbay/helpers/colors.dart';

import 'component/component_checkout/section_alamatpengiriman.dart';
import 'component/component_checkout/section_detailbelanja.dart';
import 'component/component_checkout/section_itemtocheckout.dart';
import 'component/component_checkout/section_itemtocheckout2.dart';
import 'component/component_checkout/section_pilihmetodepembayaran.dart';
import 'component/component_checkout/section_totaltagihan.dart';

class CheckOutPage extends StatefulWidget {
  CheckOutPage({Key key}) : super(key: key);

  @override
  _CheckOutPageState createState() => _CheckOutPageState();
}

class _CheckOutPageState extends State<CheckOutPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: clrbg,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text(
          "Checkout",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            SectionAlamatPengiriman(),
            SectionItemToCheckout(),
            SectionItemToCheckout2(),
            SectionPilihMetodePembayaran(),
            SectionDetailBelanja(),
            SectionTotalTagihan()
          ],
        ),
      ),
    );
  }
}
