import 'package:flutter/material.dart';
import 'package:jualbay/dummy/pengiriman.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/widget/widget_global.dart';

class PengirimanPage extends StatefulWidget {
  PengirimanPage({Key key}) : super(key: key);

  @override
  _PengirimanPageState createState() => _PengirimanPageState();
}

class _PengirimanPageState extends State<PengirimanPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: clrBg,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text(
          "Pilih Pengiriman",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          return ExpandableListView(title: dummypengiriman[index].kurir);
        },
        itemCount: dummypengiriman.length,
      ),
      floatingActionButton: PrimaryButton(
        color: clrMerahDark,
        title: 'Selesai',
        marginhorizontal: 10,
        onPress: () {},
        width: MediaQuery.of(context).size.width,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}

class ExpandableListView extends StatefulWidget {
  final String title;

  const ExpandableListView({Key key, this.title}) : super(key: key);

  @override
  _ExpandableListViewState createState() => _ExpandableListViewState();
}

class _ExpandableListViewState extends State<ExpandableListView> {
  bool expandFlag = false;

  String _currentIndex = "null";

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              border: Border(
                left:
                    BorderSide(color: clrMerahDark, width: expandFlag ? 5 : 0),
                top: BorderSide(color: clrDivider),
                bottom: BorderSide(color: clrDivider),
              ),
              color: Colors.white,
            ),
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  widget.title,
                  style: TextStyle(color: Colors.black, fontSize: 16),
                ),
                IconButton(
                    icon: Icon(
                      expandFlag
                          ? Icons.keyboard_arrow_up
                          : Icons.keyboard_arrow_down,
                      color: clrSubText,
                      size: 30.0,
                    ),
                    onPressed: () {
                      setState(() {
                        expandFlag = !expandFlag;
                      });
                    }),
              ],
            ),
          ),
          ExpandableContainer(
              expanded: expandFlag,
              child: ListView.builder(
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    decoration: BoxDecoration(
                        border: Border.all(width: 1.0, color: Colors.grey),
                        color: Colors.white),
                    child: ListTile(
                      title: Text(
                        dummypengiriman[index].type,
                        style: TextStyle(),
                      ),
                      leading: Radio(
                        activeColor: clrMerahDark,
                        value: dummypengiriman[index].type,
                        groupValue: _currentIndex,
                        onChanged: (value) {
                          setState(() {
                            _currentIndex = value;
                          });
                          print(_currentIndex);
                        },
                      ),
                      subtitle: Text("Proident officia excepteur"),
                      trailing: Text(
                        "Rp. 25.000",
                        style: TextStyle(
                            color: clrUnguDark, fontWeight: FontWeight.w600),
                      ),
                    ),
                  );
                },
                itemCount: dummypengiriman.length,
              ))
        ],
      ),
    );
  }
}
