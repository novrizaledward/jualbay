// <<<<<<< HEAD
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:jualbay/widget/widget_global.dart';
import 'package:provider/provider.dart';
import 'package:jualbay/network/api_manager.dart';

class IkmdanUmkm extends StatelessWidget {
  const IkmdanUmkm({
    Key key,
 
  }) : super(key: key);

 
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 70,
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        child: Consumer<IklanManager>(builder: (context, provider, _) {
          if (provider.modelBanner == null) {
            provider.fetchBanner();
            return CircularProgressIndicator();
          } else {
            return CachedNetworkImage(
                imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        image: DecorationImage(
                            image: imageProvider, fit: BoxFit.cover),
                      ),
                    ),
                imageUrl:
                    // "https://nos.jkt-1.neo.id/mcdonalds/promos/April2020/IsUxsSwvQpwOrMOtZLLl.jpg",
                    // "https://www.jualbuy.com/uploads/rubrik/gamebaruu.gif?time=202008060042",
                    // provider.modelBanner.result.h1.banner,
                    provider.modelBanner.result.ukm.banner,
                fit: BoxFit.contain,
                progressIndicatorBuilder: (context, url, proggres) {
                  return Center(
                    child: LoadingWidget(),
                  );
                });
          }
        }));
  }
}
// =======
// import 'package:flutter/material.dart';
// import 'package:jualbay/helpers/colors.dart';

// class IkmdanUmkm extends StatelessWidget {
//   const IkmdanUmkm({
//     Key key,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       height: 100,
//       margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
//       decoration: BoxDecoration(
//         color: Colors.white,
//         gradient: LinearGradient(
//           colors: [clrBasic1, clrBasic2],
//           begin: Alignment.bottomLeft,
//           end: Alignment.topRight,
//         ),
//         borderRadius: BorderRadius.all(Radius.circular(10)),
//         boxShadow: [
//           BoxShadow(
//             color: Colors.black.withOpacity(0.5),
//             spreadRadius: 1,
//             blurRadius: 3,
//             offset: Offset(0, 1), // changes position of shadow
//           ),
//         ],
//       ),
//       child: Center(
//         child: Text(
//           "IKM & UMKM",
//           style: TextStyle(
//             fontWeight: FontWeight.w900,
//             color: Colors.white,
//             fontSize: 20,
//           ),
//         ),
//       ),
//     );
//   }
// }
// >>>>>>> 625fb91f5671b4fae18b820fec00ec47d18cf23d
