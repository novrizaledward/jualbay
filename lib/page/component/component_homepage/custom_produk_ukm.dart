// <<<<<<< HEAD
import 'package:flutter/material.dart';
import 'package:jualbay/network/api_manager.dart';
import 'package:jualbay/widget/widget_global.dart';
import 'package:provider/provider.dart';

class ProdukUkm extends StatelessWidget {
  const ProdukUkm({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 190,
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.2),
            spreadRadius: 0.5,
            blurRadius: 5,
            offset: Offset(0, 1), // changes position of shadow
          ),
        ],
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 5),
            child: Consumer<UmkmManager>(
              builder: (context, provider, _) {
                if (provider.modelUmkm== null) {
                  provider.fetchUmkm();
                  return CircularProgressIndicator();
                } else {
                  return Container(
                    height: 170,
                    child: ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemCount: provider.modelUmkm.result.length, 
                      itemBuilder: (BuildContext context, int index) {
                        return 
                        WgtProdukView(
                          imgProduct: provider.modelUmkm.result[index].thumbs,
                          namaProduk: provider.modelUmkm.result[index].title,
                          diskonStatus:provider.modelUmkm.result[index].freeship,
                          codStatus: provider.modelUmkm.result[index].cod,
                          hargaNormal : provider.modelUmkm.result[index].salePrice,
                          hargaDiskon : provider.modelUmkm.result[index].finalPrice,
                          jmlDiskon : provider.modelUmkm.result[index].discount,
                          displayName : provider.modelUmkm.result[index].displayName,
                          idProduk : provider.modelUmkm.result[index].productId,
                        );
                      },
                    ),
                  );
                }
              },
            ),
          ),
        ),
    );
  }
}
 