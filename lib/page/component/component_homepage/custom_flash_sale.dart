// <<<<<<< HEAD
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jualbay/dummy/flashsaleList.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/network/api_constant.dart';
import 'package:jualbay/network/api_manager.dart';
import 'package:jualbay/widget/widget_global.dart';
import 'package:provider/provider.dart';

import '../../produkviewpage.dart';

class CustomFlashSale extends StatelessWidget {
  const CustomFlashSale({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        // height: 180,
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        padding: EdgeInsets.only(bottom: 10),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10)),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.2),
              spreadRadius: 0.5,
              blurRadius: 5,
              offset: Offset(0, 1), // changes position of shadow
            ),
          ],
        ),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        SvgPicture.asset("assets/svg/flashsale.svg"),
                        SizedBox(width: 10),
                        Text(
                          "FLASH SALE",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        SizedBox(width: 10),
                        Text("168:59:59")
                      ],
                    ),
                  ),
                  Text("Lihat Lainnya")
                ],
              ),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 5),
                child: Consumer<ApiManager>(
                  builder: (context, provider, _) {
                    if (provider.modelFlashSale == null) {
                      provider.fetchFlashSale();
                      return CircularProgressIndicator();
                    } else {
                      return Container(
                        height: 150,
                        child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: provider.modelFlashSale.result.length,
                          itemBuilder: (BuildContext context, int index) {
                            return FlashSaleSection(
                              img: 
                                  provider.modelFlashSale.result[index].thumbs,
                              normalPrice: provider
                                  .modelFlashSale.result[index].salePrice,
                              finalPrice: provider
                                  .modelFlashSale.result[index].finalPrice,
                              // diskonPrice: provider.modelFlashSale.result[index].discount,
                              discountType: provider
                                  .modelFlashSale.result[index].discountType,
                              idProduct: provider
                                  .modelFlashSale.result[index].productId, 
                              idProduk: provider
                                  .modelFlashSale.result[index].productId, 
                            );
                          },
                        ),
                      );
                    }
                  },
                ),
              ),
            ),
          ],
        ));
  }
}

class FlashSaleSection extends StatelessWidget {
  const FlashSaleSection({
    Key key,
    this.img,
    this.stock,
    this.diskonPrice,
    this.normalPrice,
    this.finalPrice,
    this.flashSale,
    this.discountType,
    this.cobaTesla,
    this.idProduct,
    this.idProduk,
  }) : super(key: key);

  final String img;
  final int stock;
  final dynamic diskonPrice;
  final int normalPrice;
  final int finalPrice;
  final FlashSale flashSale;
  final dynamic discountType;
  final String cobaTesla;
  final int idProduct;
  final int idProduk;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5),
      child: Column(
        children: [
          Container(
            width: 85,
            // padding: EdgeInsets.symmetric(vertical: 10),
            child: InkWell( 
              onTap: () {
               final DetailProdukProvider detailProdukProvider =
                    Provider.of<DetailProdukProvider>(context, listen: false);

                detailProdukProvider.idDetailProduk = idProduk.toString();
                detailProdukProvider.quantity = 0; 
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ProdukView(
                              // idDetailProduk: idProduk.toString(),
                            )));
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CachedNetworkImage(
                      imageBuilder: (context, imageProvider) => Container(
                            width: 85,
                            height: 85,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              image: DecorationImage(
                                  image: imageProvider, fit: BoxFit.cover),
                            ),
                          ),
                      imageUrl: img,
                      progressIndicatorBuilder: (context, url, proggres) {
                        return Center(
                          child: LoadingWidget(),
                        );
                      }),
                  SizedBox(
                    height: 5,
                  ),
                  Stack(
                    children: [
                      Container(
                        // padding: EdgeInsets.all(5),
                        height: 15,
                        decoration: BoxDecoration(
                            color: clrDivider,
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            )),
                      ),
                      Container(
                        // padding: EdgeInsets.all(5),
                        height: 15,
                        width: 70,
                        decoration: BoxDecoration( 
                            color: clrStock,
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            )),
                        child: Center(
                          child: Text(
                            discountType.toString(),
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w900,
                                fontSize: 10),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    // "Rp " + dummyFlashSale[0].diskonPrice.toString(),
                    "Rp " + finalPrice.toString(),
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16,
                      color: Colors.black,
                    ),
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  Text(
                    // "Rp " + dummyFlashSale[0].diskonPrice.toString(),
                    "Rp " + normalPrice.toString(),
                    style: TextStyle(
                      decoration: TextDecoration.lineThrough,
                      fontSize: 14,
                      color: clrSubText,
                    ),
                  ), 
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}