// <<<<<<< HEAD
import 'package:flutter/material.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/network/api_manager.dart';
import 'package:provider/provider.dart';
// import 'package:jualbay/network/api_constant.dart';
// import 'package:jualbay/widget/widget_global.dart'; 
import 'package:jualbay/widget/widget_promo.dart'; 

class PromoHariIni extends StatelessWidget {
  const PromoHariIni({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 180,
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [clrUnguLight, clrUnguDark],
          begin: Alignment.centerRight,
          end: Alignment.centerLeft,
        ),
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: Stack(
        children: [
          Positioned(
            left: 0,
            bottom: 0,
            child: Image.asset("assets/img/promo-people.png"),
          ),
          ListView(
            scrollDirection: Axis.horizontal,
            children: [
              Container(
                margin: EdgeInsets.all(15),
                width: 80,
                child: Text(
                  "PROMO "
                  "HARI INI",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 22,
                  ),
                ),
              ),
              Row(
               children:  [ 
                 Consumer<PromoManager>(
                  builder: (context, provider, _) {
                    if (provider.modelPromo == null) {
                      provider.fetchPromo();
                      return CircularProgressIndicator();
                    } else {
                      return Container(
                        height: 150,
                        child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: provider.modelPromo.result.length,
                          itemBuilder: (BuildContext context, int index ) {
                            // print(provider.modelPromo.result[index].freeship);
                            return PromoToday( 
                                imgProduct   :  provider.modelPromo.result[index].thumbs,
                                diskonStatus :  provider.modelPromo.result[index].freeship,
                                codStatus    :  provider.modelPromo.result[index].cod,
                                namaProduk   :  provider.modelPromo.result[index].title, 
                                hargaNormal  :  provider.modelPromo.result[index].salePrice,
                                hargaDiskon  :  provider.modelPromo.result[index].finalPrice,
                                jmlDiskon    :  provider.modelPromo.result[index].discount,
                                idProduk     :  provider.modelPromo.result[index].productId,
                            );
                          },
                        ),
                      );
                    }
                  },
                ),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}
// =======
// import 'package:flutter/material.dart';
// import 'package:jualbay/helpers/colors.dart';
// import 'package:jualbay/widget/widget_global.dart';

// class PromoHariIni extends StatelessWidget {
//   const PromoHariIni({
//     Key key,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       height: 180,
//       margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
//       decoration: BoxDecoration(
//         gradient: LinearGradient(
//           colors: [clrUnguLight, clrUnguDark],
//           begin: Alignment.centerRight,
//           end: Alignment.centerLeft,
//         ),
//         borderRadius: BorderRadius.all(Radius.circular(10)),
//       ),
//       child: Stack(
//         children: [
//           Positioned(
//             left: 0,
//             bottom: 0,
//             child: Image.asset("assets/img/promo-people.png"),
//           ),
//           ListView(
//             scrollDirection: Axis.horizontal,
//             children: [
//               Container(
//                 margin: EdgeInsets.all(15),
//                 width: 80,
//                 child: Text(
//                   "PROMO "
//                   "HARI INI",
//                   style: TextStyle(
//                     color: Colors.white,
//                     fontWeight: FontWeight.bold,
//                     fontSize: 22,
//                   ),
//                 ),
//               ),
//               Row(
//                 children: [
//                   WgtProdukView(
//                     imgProduct:
//                         "https://id-test-11.slatic.net/original/68ae5dbfe3526ecdffc416065fb751c7.jpg_340x340q80.jpg",
//                     diskonStatus: true,
//                     codStatus: true,
//                     namaProduk:
//                         'Original Top Blues Gold Premium Oke mantap sekali',
//                     hargaDiskon: 'Rp. 75.000',
//                     hargaNormal: 'Rp. 10.000.000',
//                     jmlDiskon: '-20%',
//                   ),
//                   WgtProdukView(
//                     imgProduct:
//                         "https://id-test-11.slatic.net/original/68ae5dbfe3526ecdffc416065fb751c7.jpg_340x340q80.jpg",
//                     diskonStatus: true,
//                     codStatus: true,
//                     namaProduk:
//                         'Original Top Blues Gold Premium Oke mantap sekali',
//                     hargaDiskon: 'Rp. 75.000',
//                     hargaNormal: 'Rp. 10.000.000',
//                     jmlDiskon: '-20%',
//                   ),
//                   WgtProdukView(
//                     imgProduct:
//                         "https://id-test-11.slatic.net/original/68ae5dbfe3526ecdffc416065fb751c7.jpg_340x340q80.jpg",
//                     diskonStatus: true,
//                     codStatus: true,
//                     namaProduk:
//                         'Original Top Blues Gold Premium Oke mantap sekali',
//                     hargaDiskon: 'Rp. 75.000',
//                     hargaNormal: 'Rp. 10.000.000',
//                     jmlDiskon: '-20%',
//                   ),
//                   WgtProdukView(
//                     imgProduct:
//                         "https://id-test-11.slatic.net/original/68ae5dbfe3526ecdffc416065fb751c7.jpg_340x340q80.jpg",
//                     diskonStatus: true,
//                     codStatus: true,
//                     namaProduk:
//                         'Original Top Blues Gold Premium Oke mantap sekali',
//                     hargaDiskon: 'Rp. 75.000',
//                     hargaNormal: 'Rp. 10.000.000',
//                     jmlDiskon: '-20%',
//                   ),
//                 ],
//               ),
//             ],
//           )
//         ],
//       ),
//     );
//   }
// }
// >>>>>>> 625fb91f5671b4fae18b820fec00ec47d18cf23d
