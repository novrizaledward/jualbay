import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:jualbay/widget/widget_global.dart';
import 'package:provider/provider.dart';
import 'package:jualbay/network/api_manager.dart';

class BannerHome extends StatelessWidget {
  const BannerHome({
    Key key,
 
  }) : super(key: key);

 
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 140,
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        child: Consumer<IklanManager>(builder: (context, provider, _) {
                  // print( provider.modelBanner.result.h1.banner);
          if (provider.modelBanner == null) {
            provider.fetchBanner();
            return CircularProgressIndicator();
          } else {
            return CachedNetworkImage(
                imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        image: DecorationImage(
                            image: imageProvider, fit: BoxFit.cover),
                      ),
                    ),
                imageUrl: provider.modelBanner.result.h1.banner.toString(),
                // fit: BoxFit.contain,
                progressIndicatorBuilder: (context, url, proggres) {
                  return Center(
                    child: LoadingWidget(),
                  );
                });
          }
        }));
  }
}