import 'dart:convert';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_svg/svg.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/network/api_manager.dart';
import 'package:jualbay/page/produkviewpage.dart';
import 'package:provider/provider.dart';
// import 'package:jualbay/network/api_manager.dart';
// import 'package:jualbay/widget/widget_global.dart';
// import 'package:provider/provider.dart';

class RekomendasiProduk extends StatefulWidget {
  const RekomendasiProduk({Key key}) : super(key: key);

  @override
  _RekomendasiProdukState createState() => _RekomendasiProdukState();
}

class _RekomendasiProdukState extends State<RekomendasiProduk> {
  bool _hasMore;
  int _pageNumber;
  bool _error;
  bool _loading;
  final int _defaultRekomsPerPageCount = 10;
  List<Rekom> _rekoms;
  final int _nextPageThreshold = 5;
  @override
  void initState() {
    super.initState();
    _hasMore = true;
    _pageNumber = 1;
    _error = false;
    _loading = true;
    _rekoms = [];
    fetchRekoms();
  }

  @override
  Widget build(BuildContext context) {
    return getBody();
  }

  Widget getBody() {
    if (_rekoms.isEmpty) {
      if (_loading) {
        return Center(
            child: Padding(
          padding: const EdgeInsets.all(8),
          child: CircularProgressIndicator(),
        ));
      } else if (_error) {
        return Center(
            child: InkWell(
          onTap: () {
            setState(() {
              _loading = true;
              _error = false;
              fetchRekoms();
            });
          },
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Text("Error while loading rekoms, tap to try agin"),
          ),
        ));
      }
    } else {
      return GridView.builder(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 4,
            childAspectRatio: 0.8,
            mainAxisSpacing: 3, 
            crossAxisSpacing: 0,
          ),
          padding: EdgeInsets.symmetric(vertical: 0),
          shrinkWrap: true,
           physics: ScrollPhysics(),
          itemCount: _rekoms.length + (_hasMore ? 1 : 0),
          itemBuilder: (context, index) {
            if (index == _rekoms.length - _nextPageThreshold) {
              fetchRekoms();
            }
            if (index == _rekoms.length) {
              if (_error) {
                return Center(
                    child: InkWell(
                  onTap: () {
                    setState(() {
                      _loading = true;
                      _error = false;
                      fetchRekoms();
                    });
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: Text("Error while loading Rekoms, tap to try agin"),
                  ),
                ));
              } else {
                return Center(
                    child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: CircularProgressIndicator(),
                ));
              }
            }
            final Rekom rekom = _rekoms[index];
            return InkWell(
              onTap: () {
                final DetailProdukProvider detailProdukProvider =
                    Provider.of<DetailProdukProvider>(context, listen: false);

                detailProdukProvider.idDetailProduk = rekom.idProduk.toString();
                detailProdukProvider.quantity = 0;
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ProdukView(
                              // idDetailProduk: rekom.idProduk.toString(),
                            )));
              },
              child: Container(
                width: 60,
                height: 100,
                margin: EdgeInsets.symmetric(horizontal: 5),
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black.withOpacity(0.3),
                      spreadRadius: 0.1,
                      blurRadius: 2,
                      offset: Offset(0, 1), // changes position of shadow
                    ),
                  ],
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                ),
                child: Column(
                  children: [
                    Container(
                      height: 65,
                      child: Stack(
                        children: [
                          Center(
                            child: CachedNetworkImage(
                              imageUrl: rekom.thumbnailUrl,
                              imageBuilder: (context, imageProvider) =>
                                  Container(
                                width: 65,
                                height: 65,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      topRight: Radius.circular(10)),
                                  image: DecorationImage(
                                      image: imageProvider, fit: BoxFit.cover),
                                ),
                              ),
                            ),
                          ),
                          rekom.discountStatus == true
                              ? Positioned(
                                  top: 0,
                                  right: 0,
                                  child: Container(
                                    height: 15,
                                    // width: 25,
                                    decoration: BoxDecoration(
                                      color: clrUnguDark,
                                      borderRadius: BorderRadius.only(
                                          bottomLeft: Radius.circular(10),
                                          topRight: Radius.circular(10)),
                                    ),
                                    child: Center(
                                      child: rekom.diskonType == "percent"
                                          ? Text(
                                              rekom.discount.toString() + "%",
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 9,
                                              ),
                                            )
                                          : Text(
                                              "Rp. " +
                                                  rekom.discount.toString(),
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 9,
                                              ),
                                            ),
                                    ),
                                  ),
                                )
                              : Container(),
                          rekom.cod == true
                              ? Positioned(
                                  bottom: 0,
                                  left: 0,
                                  child: SvgPicture.asset(
                                      "assets/svg/icon-cod.svg"),
                                )
                              : Container()
                        ],
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(1.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                              child: Container(
                                margin: EdgeInsets.only(bottom: 2),
                                child: Center(
                                  child: Text(
                                    rekom.title,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                    style: TextStyle(
                                      fontSize: 9,
                                      color: Colors.black,
                                    ),
                                  ),
                                ),
                              ),
                            ),

                            Container(
                              // margin: EdgeInsets.symmetric(vertical: 3),
                              padding: EdgeInsets.only(left: 1),
                              child: Center(
                                child: Text(
                                  "Rp. " + rekom.finalPrice.toString(),
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      fontSize: 9,
                                      color: clrUnguDark,
                                      fontWeight: FontWeight.w800),
                                ),
                              ),
                            ),
                         ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            );
          
          });
    }
    return Container();
  }

  Future<void> fetchRekoms() async {
    try {
      final response = await http.get(
          "https://api.jualbuy.com/rekom?signature=76307004d1a776aea2bb9057d50e960f789bf54c&request_type=scroll&page=$_pageNumber");
      List<Rekom> fetchedRekoms = Rekom.parseList(json.decode(response.body));
      setState(() {
        _hasMore = fetchedRekoms.length == _defaultRekomsPerPageCount;
        _loading = false;
        _pageNumber = _pageNumber + 1;
        _rekoms.addAll(fetchedRekoms);
      });
    } catch (e) {
      setState(() {
        _loading = false;
        _error = true;
      });
    }
  }
}

class Rekom {
  final String title;
  final String thumbnailUrl;
  final int salePrice;
  final bool cod;
  final int finalPrice;
  // final dynamic diskonStatus;
  final dynamic discount;
  final dynamic diskonType;
  final bool discountStatus;
  final int idProduk;
  final int idCategory;

  Rekom(
    this.title,
    this.thumbnailUrl,
    this.salePrice,
    this.cod,
    this.finalPrice,
    // this.diskonStatus,
    this.discount,
    this.diskonType,
    this.discountStatus,
    this.idProduk,
    this.idCategory,
  );

  factory Rekom.fromJson(Map<String, dynamic> json) {
    return Rekom(
      json["title"],
      json["thumbs"],
      json['sale_price'],
      json['cod'],
      json['final_price'],
      json["discount"],
      json['discount_type'],
      json["discount_status"],
      json["product_id"],
      json["category_id"],
    );
  }
  static List<Rekom> parseList(List<dynamic> list) {
    return list.map((i) => Rekom.fromJson(i)).toList();
  }
}
