// <<<<<<< HEAD
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jualbay/helpers/colors.dart';

class InfoSaldo extends StatelessWidget {
  const InfoSaldo({
    Key key,
    this.marginTop = 180,
  }) : super(key: key);

  final double marginTop;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      margin: EdgeInsets.fromLTRB(10, marginTop, 10, 5),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.5),
            spreadRadius: 1,
            blurRadius: 3,
            offset: Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Container(
              alignment: Alignment.center,
              child: SvgPicture.asset("assets/svg/icon-scan.svg"),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: VerticalDivider(
              color: clrDivider,
              width: 1,
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SvgPicture.asset(
                          "assets/svg/account-balance-wallet-24px.svg",
                          width: 20,
                          height: 20,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          "Rp. 150.000",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                            color: Colors.black,
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    child: Text(
                      "Saldo jB-Pay",
                      style: TextStyle(
                        // fontWeight: FontWeight.,
                        fontSize: 16,
                        color: clrSubText,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: VerticalDivider(
              color: clrDivider,
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SvgPicture.asset(
                          "assets/svg/local-play-24px.svg",
                          width: 20,
                          height: 20,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          "15 Kupon",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                            color: Colors.black,
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    child: Text(
                      "Kupon Saya",
                      style: TextStyle(
                        // fontWeight: FontWeight.,
                        fontSize: 16,
                        color: clrSubText,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
// =======
// import 'package:flutter/material.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// import 'package:jualbay/helpers/colors.dart';

// class InfoSaldo extends StatelessWidget {
//   const InfoSaldo({
//     Key key,
//     this.marginTop = 180,
//   }) : super(key: key);

//   final double marginTop;
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       height: 60,
//       margin: EdgeInsets.fromLTRB(10, marginTop, 10, 5),
//       decoration: BoxDecoration(
//         color: Colors.white,
//         borderRadius: BorderRadius.all(Radius.circular(10)),
//         boxShadow: [
//           BoxShadow(
//             color: Colors.black.withOpacity(0.5),
//             spreadRadius: 1,
//             blurRadius: 3,
//             offset: Offset(0, 1), // changes position of shadow
//           ),
//         ],
//       ),
//       child: Row(
//         children: [
//           Expanded(
//             flex: 1,
//             child: Container(
//               alignment: Alignment.center,
//               child: SvgPicture.asset("assets/svg/icon-scan.svg"),
//             ),
//           ),
//           Padding(
//             padding: EdgeInsets.symmetric(vertical: 10),
//             child: VerticalDivider(
//               color: clrDivider,
//               width: 1,
//             ),
//           ),
//           Expanded(
//             flex: 2,
//             child: Container(
//               margin: EdgeInsets.symmetric(horizontal: 10),
//               child: Column(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   Container(
//                     child: Row(
//                       mainAxisAlignment: MainAxisAlignment.start,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         SvgPicture.asset(
//                           "assets/svg/account-balance-wallet-24px.svg",
//                           width: 20,
//                           height: 20,
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Text(
//                           "Rp. 150.000",
//                           style: TextStyle(
//                             fontWeight: FontWeight.bold,
//                             fontSize: 16,
//                             color: Colors.black,
//                           ),
//                         )
//                       ],
//                     ),
//                   ),
//                   Container(
//                     child: Text(
//                       "Saldo jB-Pay",
//                       style: TextStyle(
//                         // fontWeight: FontWeight.,
//                         fontSize: 16,
//                         color: clrSubText,
//                       ),
//                     ),
//                   )
//                 ],
//               ),
//             ),
//           ),
//           Padding(
//             padding: EdgeInsets.symmetric(vertical: 10),
//             child: VerticalDivider(
//               color: clrDivider,
//             ),
//           ),
//           Expanded(
//             flex: 2,
//             child: Container(
//               margin: EdgeInsets.symmetric(horizontal: 10),
//               child: Column(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   Container(
//                     child: Row(
//                       mainAxisAlignment: MainAxisAlignment.start,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         SvgPicture.asset(
//                           "assets/svg/local-play-24px.svg",
//                           width: 20,
//                           height: 20,
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Text(
//                           "15 Kupon",
//                           style: TextStyle(
//                             fontWeight: FontWeight.bold,
//                             fontSize: 16,
//                             color: Colors.black,
//                           ),
//                         )
//                       ],
//                     ),
//                   ),
//                   Container(
//                     child: Text(
//                       "Kupon Saya",
//                       style: TextStyle(
//                         // fontWeight: FontWeight.,
//                         fontSize: 16,
//                         color: clrSubText,
//                       ),
//                     ),
//                   )
//                 ],
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }
// >>>>>>> 625fb91f5671b4fae18b820fec00ec47d18cf23d
