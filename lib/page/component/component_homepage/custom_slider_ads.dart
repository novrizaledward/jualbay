// <<<<<<< HEAD
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:jualbay/helpers/colors.dart';

class SliderAds extends StatefulWidget {
  const SliderAds({
    Key key,
  }) : super(key: key);

  @override
  _SliderAdsState createState() => _SliderAdsState();
}

class _SliderAdsState extends State<SliderAds> {
  int _current = 1;
  @override
  Widget build(BuildContext context) {
    List<int> list = [1, 2, 3, 4, 5];

    return Stack(
      children: [
        Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10)),
              gradient: LinearGradient(
                colors: [clrBasic1, clrBasic2],
                begin: Alignment.bottomLeft,
                end: Alignment.topRight,
              ),
              // borderRadius: BorderRadius.all(Radius.circular(10)),
              boxShadow: [
                BoxShadow(
                  color: Colors.black.withOpacity(0.5),
                  spreadRadius: 1,
                  blurRadius: 3,
                  offset: Offset(0, 1), // changes position of shadow
                ),
              ],
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              child: CarouselSlider(
                options: CarouselOptions(
                    height: 100,
                    viewportFraction: 1,
                    onPageChanged: (index, c) {
                      setState(() {
                        _current = index;
                        // print(index);
                        // print(_current);
                      });
                    }),
                items: list
                    .map((item) => Container(
                          child:
                              Center(child: Text("Slide " + item.toString())),
                          decoration: BoxDecoration(
                            gradient: LinearGradient(
                              colors: [clrBasic1, clrBasic2],
                              begin: Alignment.bottomLeft,
                              end: Alignment.topRight,
                            ),
                          ),
                        ))
                    .toList(),
              ),
            )),
        Positioned(
          left: 20,
          bottom: 10,
          child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: []..addAll(list.map((e) {
                  print(e);
                  return Container(
                    width: _current == e ? 12 : 8,
                    height: _current == e ? 12 : 8,
                    margin:
                        EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: _current == e
                            ? Colors.white.withOpacity(0.9)
                            : Colors.white.withOpacity(0.4)),
                  );
                }).toList())),
        ),
      ],
    );
  }
}
