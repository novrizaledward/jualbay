// <<<<<<< HEAD
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jualbay/helpers/colors.dart';
// import 'package:jualbay/page/component/component_homepage/list_unli.dart';
// import 'package:jualbay/page/component/component_produkview/section_untukkamu.dart';
// import 'package:jualbay/widget/widget_scroll.dart';
// import 'package:jualbay/page/component/component_homepage/custom_rekomendasisection.dart';

// import 'custom_kategori_list.dart';
import 'rekomendasi_produk.dart';
class RekomendasiSection extends StatefulWidget {
  const RekomendasiSection({
    Key key,
  }) : super(key: key);

  @override
  _RekomendasiSectionState createState() => _RekomendasiSectionState();
}

class _RekomendasiSectionState extends State<RekomendasiSection>
    with TickerProviderStateMixin {
  TabController _tabController;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Row(
            children: [
              SvgPicture.asset("assets/svg/icon-star.svg"),
              SizedBox(
                width: 10,
              ),
              Text(
                "UNTUK KAMU",
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w900,
                    fontSize: 18),
              )
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.only(),
          child: TabBar(
            indicatorColor: clrSubText,
            indicatorSize: TabBarIndicatorSize.label,
            indicatorWeight: 5,
            isScrollable: true,
            controller: _tabController,
            labelPadding: EdgeInsets.symmetric(horizontal: 5),
            tabs: [
              Container(
                width: MediaQuery.of(context).size.width / 6,
                height: 70,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(5),
                    topRight: Radius.circular(5),
                  ),
                  color: clrPgView1,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset(
                      "assets/svg/icon-recom.svg",
                    ),
                    Text(
                      "Recom",
                      style:
                          TextStyle(fontWeight: FontWeight.w700, fontSize: 16),
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 6,
                height: 70,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(5)),
                  color: clrPgView2,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset("assets/svg/icon-codtab.svg"),
                    Text("COD"),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 6,
                height: 70,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(5)),
                  color: clrPgView3,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset("assets/svg/icon-shipping.svg"),
                    Text("Shipping"),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 6,
                height: 70,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(5)),
                  color: clrPgView4,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset("assets/svg/icon-diskon.svg"),
                    Text("Diskon"),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 6,
                height: 70,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(5)),
                  color: clrPgView5,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SvgPicture.asset("assets/svg/icon-recent.svg"),
                    Text("Recent"),
                  ],
                ),
              ),
            ],
          ),
        ),
          Container(
            height: 430,
            child: TabBarView(
              children: [
                RekomendasiProduk(),
                // HomeScreen(),
                // SectionUntukKamu(),
              
                Center(
                  child: Text("data 1"),
                ),
                Center(
                  child: Text("data 1"),
                ),
                Center(
                  child: Text("data 1"),
                ),
                Center(
                  child: Text("data 1"),
                ),
              ],
            ),
          )
        ],
      
    );
  }
} 