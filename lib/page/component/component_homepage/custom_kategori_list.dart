// <<<<<<< HEAD
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
// import 'package:jualbay/dummy/kategoriList.dart';
import 'package:flutter/cupertino.dart';
// import 'package:jualbay/page/component/component_homepage/kategori_extract.dart';
import 'package:provider/provider.dart';
import 'package:jualbay/network/api_manager.dart';

class KategoriList extends StatelessWidget {
  const KategoriList({
    Key key,
    this.imgProduct = "",
    this.link = "",
    this.displayName = "",
  }) : super(key: key);

  final String imgProduct;
  final String link;
  final String displayName;
  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.cyanAccent,
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      child: Consumer<ListManager>(
        builder: (context, provider, _) {
          if (provider.modelShortcut == null) {
            provider.fetchShortcut();
            return CircularProgressIndicator(); 
          } else {
            return Container(
              child: GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 5,
                  childAspectRatio: 0.8,
                  mainAxisSpacing: 5,
                  crossAxisSpacing: 0,
                ),
                 padding: EdgeInsets.symmetric(vertical: 0),
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: provider.modelShortcut.result.length,
                itemBuilder: (BuildContext context, int index) { 
                      return Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center, 
                          children: [
                            Expanded(
                              flex: 2,
                              child: Container(
                                margin: EdgeInsets.only(bottom: 5),
                                // child: SvgPicture.asset(iconKategori[index]),
                                child:  SvgPicture.network(provider.modelShortcut.result[index].icon),
                              ),
                            ),
                            Expanded( 
                              flex: 1,
                              child: Container(
                                child: Text(
                                   provider.modelShortcut.result[index].title,
                                  // displayName,
                                  style: TextStyle(),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                  },
              ),
            );
          }
        },
      ),
    );
  }
} 