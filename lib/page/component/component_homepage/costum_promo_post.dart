import 'package:flutter/material.dart'; 
import 'package:jualbay/model/postModel.dart';
import 'package:jualbay/network/api_constant.dart';
import 'package:jualbay/network/api_repository.dart';
import 'package:jualbay/widget/widget_promo.dart';
 

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  ApiRepository _apiRepository = ApiRepository();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: FutureBuilder(
          future: _apiRepository.getDataPostFromApi,
          builder:
              (BuildContext context, AsyncSnapshot<PostModel> snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                return Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                );
                break;
              case ConnectionState.waiting:
                return Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                );
                break;
              case ConnectionState.active:
                return Container(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                );
                break;
              case ConnectionState.done:
                if (snapshot.hasError) {
                  return Container(
                    child: Center(
                      child: Text("Something Wrong"),
                    ),
                  );
                } else {
                  return Build(
                    listData: snapshot.data,
                  );
                }
                break;
            }
            return Container();
          },
        ),
      ),
    );
  }
}

class Build extends StatelessWidget {
  const Build({Key key, 
  
  this.listData}) : super(key: key);

  final  PostModel listData;
  @override
  Widget build(BuildContext context) {
     return Container(
      child: ListView.builder(
        itemCount: listData.result.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(listData.result[index].title),
            subtitle: Text(ApiConstant.baseUrl + listData.result[index].thumbs + " |Nama Toko : " + listData.result[index].displayName),
          );
        },
      ),
    );
  }
}

 