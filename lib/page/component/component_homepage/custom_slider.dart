// <<<<<<< HEAD
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:jualbay/network/api_manager.dart';
// import 'package:jualbay/widget/widget_global.dart';
import 'package:provider/provider.dart';
// import 'package:jualbay/network/api_constant.dart';

class CustomSliderHome extends StatefulWidget {
  const CustomSliderHome({
    Key key,
    this.imgSlider,
    this.marginTop,
  }) : super(key: key);

  final double marginTop;
  final String imgSlider;
  @override
  _CustomSliderHomeState createState() => _CustomSliderHomeState();
}

class _CustomSliderHomeState extends State<CustomSliderHome> {
  @override
  Widget build(BuildContext context) {
    int _current = 0;
    return Stack(
      children: [
        CarouselSlider(
          items: [
            Consumer<SliderManager>(
              builder: (context, provider, _) {
                if (provider.modelSlider == null) {
                  provider.fetchSlider();
                  return CircularProgressIndicator();
                } else {
                  return Container(
                    height: 150,
                    child: ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemCount: provider.modelSlider.result.length,
                      itemBuilder: (BuildContext context, int index) {                      
                        return Container(
                          width: MediaQuery.of(context).size.width,
                          child: CachedNetworkImage(
                              fit: BoxFit.fitHeight, 
                              imageUrl:   provider.modelSlider.result[index].thumbs,                       
                          ),
                        );
                      },
                    ),
                  );
                }
              },
            ), 
          ],
          options: CarouselOptions(
              autoPlay: false,
              enlargeCenterPage: false,
              viewportFraction: 1,
              height: 200,
              onPageChanged: (index, reason) {
                setState(() {
                  _current = index;
                });
              }),
        ),
        Positioned(
          left: 20,
          bottom: 20,
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Container(
              width: 8.0,
              height: 8.0,
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
              decoration: BoxDecoration(
                  shape: BoxShape.circle, color: Color.fromRGBO(0, 0, 0, 1)),
            ),
            Container(
              width: 8.0,
              height: 8.0,
              margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
              decoration: BoxDecoration(
                  shape: BoxShape.circle, color: Color.fromRGBO(0, 0, 0, 0.4)),
            ),
          ]),
        ),
      ],
    );
  }
} 