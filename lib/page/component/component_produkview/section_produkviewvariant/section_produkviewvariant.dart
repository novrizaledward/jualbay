import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jualbay/dummy/flashsaleList.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/helpers/constant.dart';
import 'package:jualbay/widget/widget_global.dart';

class ProdukViewVariant extends StatefulWidget {
  ProdukViewVariant({Key key}) : super(key: key);

  @override
  _ProdukViewVariantState createState() => _ProdukViewVariantState();
}

class _ProdukViewVariantState extends State<ProdukViewVariant> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFF737373),
      child: Container(
          height: 580,
          padding: EdgeInsets.only(top: 10, left: 10, right: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10)),
            color: Colors.white,
          ),
          child: Stack(
            children: [
              Column(
                children: [
                  SvgPicture.asset("assets/svg/produkviewkurir/line.svg"),
                  SizedBox(
                    height: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: 5, vertical: 10),
                            width: 75,
                            height: 75,
                            child: CachedNetworkImage(
                                imageBuilder: (context, imageProvider) =>
                                    Container(
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          color: clrDivider,
                                        ),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(5)),
                                        image: DecorationImage(
                                            image: imageProvider,
                                            fit: BoxFit.cover),
                                      ),
                                    ),
                                imageUrl: dummyFlashSale[0].img,
                                progressIndicatorBuilder:
                                    (context, url, proggres) {
                                  return Center(
                                    child: LoadingWidget(),
                                  );
                                }),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            height: 75,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  "RP. 75.000",
                                  style: stylePrice,
                                ),
                                Row(
                                  children: [
                                    Text(
                                      "RP. 75.000",
                                      style: styleDiskon.copyWith(
                                          color: clrSubText, fontSize: 16),
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Container(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 3, vertical: 1),
                                      decoration: BoxDecoration(
                                          color: clrMerahDark,
                                          borderRadius:
                                              BorderRadius.circular(3)),
                                      child: Text(
                                        "-5%",
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    SvgPicture.asset(
                                        "assets/svg/produkviewvariant/buttonminuns.svg"),
                                    SizedBox(
                                      width: 15,
                                    ),
                                    Text(
                                      "5",
                                      style: TextStyle(color: clrSubText),
                                    ),
                                    SizedBox(
                                      width: 15,
                                    ),
                                    SvgPicture.asset(
                                        "assets/svg/produkviewvariant/buttonplus.svg"),
                                  ],
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                      Divider(
                        color: clrDivider,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Text(
                            "VARIASI :  ",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          ),
                          Text("Biru")
                        ],
                      ),
                      Container(
                        height: 80,
                        child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: 8,
                          itemBuilder: (BuildContext context, int index) {
                            return InkWell(
                              onTap: () {},
                              child: Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 10),
                                width: 65,
                                height: 65,
                                child: CachedNetworkImage(
                                    imageBuilder: (context, imageProvider) =>
                                        Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              color: clrDivider,
                                            ),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5)),
                                            image: DecorationImage(
                                                image: imageProvider,
                                                fit: BoxFit.cover),
                                          ),
                                        ),
                                    imageUrl: dummyFlashSale[index].img,
                                    progressIndicatorBuilder:
                                        (context, url, proggres) {
                                      return Center(
                                        child: LoadingWidget(),
                                      );
                                    }),
                              ),
                            );
                          },
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Text(
                            "UKURAN :  ",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          ),
                          Text("500 ml")
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(right: 10),
                            padding: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 10),
                            decoration: BoxDecoration(
                              border: Border.all(color: clrMerahDark),
                              color: clrButtonbg,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: Text("100 ml"),
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 10),
                            padding: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 10),
                            decoration: BoxDecoration(
                              color: clrButtonbg,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: Text("250 ml"),
                          ),
                          Container(
                            margin: EdgeInsets.only(right: 10),
                            padding: EdgeInsets.symmetric(
                                vertical: 5, horizontal: 10),
                            decoration: BoxDecoration(
                              color: clrButtonbg,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: Text("500 ml"),
                          ),
                        ],
                      )
                    ],
                  ),
                ],
              ),
              Positioned(
                bottom: 10,
                left: 0,
                right: 0,
                child: Row(
                  children: [
                    SvgPicture.asset(
                        "assets/svg/produkviewvariant/buttonchat.svg"),
                    Expanded(
                      child: PrimaryButton(
                        color: clrUnguDark,
                        marginhorizontal: 5,
                        title: '+ Keranjang',
                        borderColor: clrUnguDark,
                        onPress: () {},
                      ),
                    ),
                    Expanded(
                      child: PrimaryButton(
                        color: clrMerahDark,
                        marginhorizontal: 5,
                        title: 'Beli Sekarang',
                        onPress: () {},
                      ),
                    ),
                  ],
                ),
              )
            ],
          )),
    );
  }
}
