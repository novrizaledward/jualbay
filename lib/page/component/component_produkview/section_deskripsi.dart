// <<<<<<< HEAD
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/widget/widget_global.dart';

class SectionDeskripsi extends StatelessWidget {
  const SectionDeskripsi({
    Key key,
    this.deskripsiProduk,
    this.beratProduk,
    this.minimalPesan,
    this.kategoriProduk,
    this.kondisiProduk
  }) : super(key: key);

final String deskripsiProduk; 
final String beratProduk;
final String minimalPesan; 
final String kategoriProduk;
final String kondisiProduk;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(top: 5),
      color: Colors.white,
      child: Column(
        children: [
          TitleOfSection(
            title: "DESKRIPSI",
            insertRow: false,
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Berat"),
              Text( beratProduk + "Gr"),
            ],
          ),
          Divider(
            color: clrDivider,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Kondisi"),

              kondisiProduk.toString() == "1" ?
              Text("Baru")
               : Text("Bekas")
            ],
          ),
          Divider(
            color: clrDivider,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Minimal Pemesanan"),
              Text(minimalPesan + "item"),
            ],
          ),
          Divider(
            color: clrDivider,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Kategori"),
              Text(kategoriProduk),
            ],
          ),
          Divider(
            color: clrDivider,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Text(
                  // "loreMollit aliquip labore adipisicing nulla aute duis deserunt. Laboris cillum qui laborum sunt ad ullamco reprehenderit deserunt velit aute. Do est enim nisi exercitation ullamco nulla ullamco ullamco esse et qui fugiat enim aliqua.Nulla id laborum veniam Lorem consequat. Reprehenderit aliquip incididunt cupidatat exercitation. Eu aliqua anim sunt incididunt exercitation dolore labore pariatur ex cillum nulla. Id aliquip et reprehenderit aute consequat fugiat est veniam reprehenderit mollit. Voluptate exercitation quis ut id ex esse eu elit sit fugiat do ad enim proident.",
                  deskripsiProduk,
                  maxLines: 4,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(right: 0),
                child: SvgPicture.asset(
                    "assets/svg/productview/icon-arrownext.svg"),
              )
            ],
          ),
        ],
      ),
    );
  }
}