// <<<<<<< HEAD
import 'package:flutter/material.dart';
import 'package:jualbay/network/api_manager.dart';
// import 'package:jualbay/widget/widget_global.dart';
import 'package:provider/provider.dart';

class SectionPromo extends StatelessWidget {
  const SectionPromo({
    Key key,
    this.promoToko,
  }) : super(key: key);

  final dynamic promoToko;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(top: 5),
      color: Colors.white,
      child: Consumer<DetailProdukProvider>(builder: (context, provider, _) {
        return
            //   Column(
            // children: [
            //   TitleOfSection(
            //     title: "PROMO",
            //     insertRow: false,
            //   ),
            Container(
          height: 80,
          child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: provider.detailProdukModel.reslut[0].voucher.length,
            itemBuilder: (BuildContext context, int index) {
              // print(provider.detailProdukModel.reslut[0].voucher[index].code);
              return
               provider.detailProdukModel.reslut[0].voucher[index].toString() != ""
                  ? Container(
                      height: 60,
                      padding: EdgeInsets.only(top: 5),
                      width: MediaQuery.of(context).size.width,
                      child: ListView(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        children: [
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 5),
                            child: Stack(
                              children: [
                                Image.asset(
                                  "assets/img/productview/logo-cashback.png",
                                  fit: BoxFit.fill,
                                  height: 50,
                                ),
                                Container(
                                  margin: EdgeInsets.symmetric(
                                      vertical: 5, horizontal: 15),
                                  child: RichText(
                                    text: TextSpan(children: <TextSpan>[
                                      TextSpan(
                                        text: "Code Promo \n",
                                        style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.w700,
                                          color: Colors.white,
                                        ),
                                      ),
                                      TextSpan(
                                        text: provider.detailProdukModel
                                            .reslut[0].voucher[0].code,
                                        style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w700,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ]),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ))
                  : Container(
                      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
                    );
              //   ],
              // );
            },
          ),
        );
      }),
    );
  }
}
