import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/page/component/component_produkview/section_produkviewulasan/sectionSliderRatingBar.dart';
import 'package:jualbay/widget/widget_global.dart';

class ProdukViewUlasan extends StatefulWidget {
  ProdukViewUlasan({Key key}) : super(key: key);

  @override
  _ProdukViewUlasanState createState() => _ProdukViewUlasanState();
}

class _ProdukViewUlasanState extends State<ProdukViewUlasan> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: clrbg,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text(
          "Penilaian",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: Column(
        children: [
          IntrinsicHeight(
            child: Container(
              color: Colors.white,
              child: Row(
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                    child: Column(
                      children: [
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: '4.5',
                                style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 40,
                                ),
                              ),
                              TextSpan(
                                text: '/',
                                style: TextStyle(
                                    color: Colors.black, fontSize: 25),
                              ),
                              TextSpan(
                                text: '5',
                                style: TextStyle(
                                    color: Colors.black, fontSize: 25),
                              ),
                            ],
                          ),
                        ),
                        Row(
                          children: [
                            SvgPicture.asset(
                              "assets/svg/productview/icon-stars.svg",
                              width: 20,
                            ),
                            Text(
                              "80 Ulasan",
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  VerticalDivider(
                    color: clrDivider,
                  ),
                  SectionSlideRatingBar(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
