import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:jualbay/widget/widget_global.dart';

class SectionSlideRatingBar extends StatelessWidget {
  const SectionSlideRatingBar({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double sliderWidth = MediaQuery.of(context).size.width;
    return Expanded(
      child: Container(
        height: 80,
        padding: EdgeInsets.only(top: 10, left: 5, right: 10, bottom: 0),
        child: ListView.builder(
          cacheExtent: 0,
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          itemCount: 5,
          itemBuilder: (BuildContext context, int index) {
            return Row(
              children: [
                RatingBar.builder(
                  initialRating: 10,
                  minRating: 0,
                  itemSize: 10,
                  maxRating: 100,
                  direction: Axis.horizontal,
                  allowHalfRating: false,
                  itemCount: 5,
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  onRatingUpdate: (rating) {
                    print(rating);
                  },
                ),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 5),
                    child: CustomSlider(
                      percentage: 50 / sliderWidth,
                      width: sliderWidth,
                    ),
                  ),
                ),
                Text(
                  "50",
                  style: TextStyle(color: Colors.black),
                )
              ],
            );
          },
        ),
      ),
    );
  }
}
