// <<<<<<< HEAD
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/network/api_manager.dart';
import 'package:jualbay/widget/widget_global.dart';
import 'package:provider/provider.dart';

class SectionTanyaProduk extends StatelessWidget {
  const SectionTanyaProduk({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.only(top: 5),
        color: Colors.white,
        child: Consumer<DetailProdukProvider>(builder: (context, provider, _) {
          return
              Container(
            height: 110,
            child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              itemCount: provider.detailProdukModel.reslut[0].comment.length,
              itemBuilder: (BuildContext context, int index) {
                return
                    Column(
                  children: [
                    TitleOfSection(
                      title: "TANYA PRODUK",
                      insertRow: false,
                      action: "Lihat Semua",
                      ontap: () {},
                    ),
                    
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        CircleAvatar(
                          backgroundColor: clrDivider,
                          radius: 10,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          provider.detailProdukModel.reslut[0].comment[index].userStatus,
                          // "User",  
                          style: TextStyle(color: Colors.black),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        SvgPicture.asset("assets/svg/productview/icon-dot.svg"),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          // "5 Menit Lalu"
                          provider.detailProdukModel.reslut[0].comment[index].tanggal.toString(),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      // "Laboris amet ipsum veniam deserunt sint incididunt incididunt excepteur officia. Lorem nostrud adipisicing duis aute laboris irure irure et sit sunt amet anim. Officia velit et pariatur ex aute nisi tempor Lorem sit minim cupidatat in. Lorem quis quis qui ad pariatur deserunt ipsum consequat anim esse Lorem anim mollit aliquip. Qui dolore eu elit Lorem ea laborum aliquip Lorem in dolor veniam fugiat. Ea enim officia sit qui officia quis. Irure ut fugiat labore laboris tempor velit est sunt non ea sit eiusmod.",
                      provider.detailProdukModel.reslut[0].comment[index].komenIsi,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                        height: 20,
                        child: Row(
                          children: [
                            VerticalDivider(
                              color: clrDivider,
                              thickness: 3,
                              endIndent: 1,
                              indent: 1,
                              width: 1,
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              "2 Jawaban Lainnya",
                              style: TextStyle(color: Colors.black),
                            ),
                          ],
                        )),
                  ],
                );
              },
            ),
          );
        }));
  }
}
