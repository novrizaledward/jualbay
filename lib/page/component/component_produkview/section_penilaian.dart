// <<<<<<< HEAD
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jualbay/dummy/flashsaleList.dart';
import 'package:jualbay/widget/widget_global.dart';

class SectionPenilaian extends StatelessWidget {
  const SectionPenilaian({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(top: 5),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TitleOfSection(
            title: "PENILAIAN",
            insertRow: true,
            action: "Lihat Semua",
            ontap: () {},
          ),
          Container(
            height: 80,
            child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: 5,
              itemBuilder: (BuildContext context, int index) {
                return Stack(
                  children: [
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                      width: 55,
                      height: 55,
                      child: CachedNetworkImage(
                          imageBuilder: (context, imageProvider) => Container(
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(5)),
                                  image: DecorationImage(
                                      image: imageProvider, fit: BoxFit.cover),
                                ),
                              ),
                          imageUrl: dummyFlashSale[index].img,
                          progressIndicatorBuilder: (context, url, proggres) {
                            return Center(
                              child: LoadingWidget(),
                            );
                          }),
                    ),
                    (index == 4)
                        ? Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: 5, vertical: 10),
                            width: 55,
                            height: 55,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              color: Colors.black.withOpacity(0.6),
                            ),
                            child: Center(
                              child: Text(
                                "Lainnya \n+35",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 16,
                                ),
                              ),
                            ),
                          )
                        : Container()
                  ],
                );
              },
            ),
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: [
              Text(
                "User",
                style: TextStyle(color: Colors.black),
              ),
              SizedBox(
                width: 5,
              ),
              SvgPicture.asset("assets/svg/productview/icon-dot.svg"),
              SizedBox(
                width: 5,
              ),
              Text("1 Jam Lalu")
            ],
          ),
          SizedBox(
            height: 10,
          ),
          RatingBar.builder(
            initialRating: 4.5,
            minRating: 1,
            itemSize: 15,
            maxRating: 10,
            direction: Axis.horizontal,
            allowHalfRating: true,
            itemCount: 5,
            itemBuilder: (context, _) => Icon(
              Icons.star,
              color: Colors.amber,
            ),
            onRatingUpdate: (rating) {
              print(rating);
            },
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            "Laboris amet ipsum veniam deserunt sint incididunt incididunt excepteur officia. Lorem nostrud adipisicing duis aute laboris irure irure et sit sunt amet anim. Officia velit et pariatur ex aute nisi tempor Lorem sit minim cupidatat in. Lorem quis quis qui ad pariatur deserunt ipsum consequat anim esse Lorem anim mollit aliquip. Qui dolore eu elit Lorem ea laborum aliquip Lorem in dolor veniam fugiat. Ea enim officia sit qui officia quis. Irure ut fugiat labore laboris tempor velit est sunt non ea sit eiusmod.",
            maxLines: 3,
            overflow: TextOverflow.ellipsis,
          )
        ],
      ),
    );
  }
}
// =======
// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_rating_bar/flutter_rating_bar.dart';
// import 'package:flutter_svg/flutter_svg.dart';
// import 'package:jualbay/dummy/flashsaleList.dart';
// import 'package:jualbay/page/component/component_produkview/section_produkviewulasan/section_produkviewulasan.dart';
// import 'package:jualbay/widget/widget_global.dart';

// class SectionPenilaian extends StatelessWidget {
//   const SectionPenilaian({
//     Key key,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: EdgeInsets.all(10),
//       margin: EdgeInsets.only(top: 5),
//       color: Colors.white,
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           TitleOfSection(
//             title: "PENILAIAN",
//             insertRow: true,
//             action: "Lihat Semua",
//             ontap: () {
//               Navigator.push(context,
//                   MaterialPageRoute(builder: (context) => ProdukViewUlasan()));
//             },
//           ),
//           Container(
//             height: 80,
//             child: ListView.builder(
//               shrinkWrap: true,
//               scrollDirection: Axis.horizontal,
//               itemCount: 5,
//               itemBuilder: (BuildContext context, int index) {
//                 return Stack(
//                   children: [
//                     Container(
//                       margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
//                       width: 55,
//                       height: 55,
//                       child: CachedNetworkImage(
//                           imageBuilder: (context, imageProvider) => Container(
//                                 decoration: BoxDecoration(
//                                   borderRadius:
//                                       BorderRadius.all(Radius.circular(5)),
//                                   image: DecorationImage(
//                                       image: imageProvider, fit: BoxFit.cover),
//                                 ),
//                               ),
//                           imageUrl: dummyFlashSale[index].img,
//                           progressIndicatorBuilder: (context, url, proggres) {
//                             return Center(
//                               child: LoadingWidget(),
//                             );
//                           }),
//                     ),
//                     (index == 4)
//                         ? Container(
//                             margin: EdgeInsets.symmetric(
//                                 horizontal: 5, vertical: 10),
//                             width: 55,
//                             height: 55,
//                             decoration: BoxDecoration(
//                               borderRadius:
//                                   BorderRadius.all(Radius.circular(5)),
//                               color: Colors.black.withOpacity(0.6),
//                             ),
//                             child: Center(
//                               child: Text(
//                                 "Lainnya \n+35",
//                                 textAlign: TextAlign.center,
//                                 style: TextStyle(
//                                   color: Colors.white,
//                                   fontSize: 16,
//                                 ),
//                               ),
//                             ),
//                           )
//                         : Container()
//                   ],
//                 );
//               },
//             ),
//           ),
//           SizedBox(
//             height: 5,
//           ),
//           Row(
//             children: [
//               Text(
//                 "User",
//                 style: TextStyle(color: Colors.black),
//               ),
//               SizedBox(
//                 width: 5,
//               ),
//               SvgPicture.asset("assets/svg/productview/icon-dot.svg"),
//               SizedBox(
//                 width: 5,
//               ),
//               Text("1 Jam Lalu")
//             ],
//           ),
//           SizedBox(
//             height: 10,
//           ),
//           RatingBar(
//             initialRating: 4.5,
//             minRating: 1,
//             itemSize: 15,
//             maxRating: 10,
//             direction: Axis.horizontal,
//             allowHalfRating: true,
//             itemCount: 5,
//             itemBuilder: (context, _) => Icon(
//               Icons.star,
//               color: Colors.amber,
//             ),
//             onRatingUpdate: (rating) {
//               print(rating);
//             },
//           ),
//           SizedBox(
//             height: 10,
//           ),
//           Text(
//             "Laboris amet ipsum veniam deserunt sint incididunt incididunt excepteur officia. Lorem nostrud adipisicing duis aute laboris irure irure et sit sunt amet anim. Officia velit et pariatur ex aute nisi tempor Lorem sit minim cupidatat in. Lorem quis quis qui ad pariatur deserunt ipsum consequat anim esse Lorem anim mollit aliquip. Qui dolore eu elit Lorem ea laborum aliquip Lorem in dolor veniam fugiat. Ea enim officia sit qui officia quis. Irure ut fugiat labore laboris tempor velit est sunt non ea sit eiusmod.",
//             maxLines: 3,
//             overflow: TextOverflow.ellipsis,
//           )
//         ],
//       ),
//     );
//   }
// }
// >>>>>>> 625fb91f5671b4fae18b820fec00ec47d18cf23d
