// <<<<<<< HEAD
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:jualbay/network/api_manager.dart';
// import 'package:jualbay/network/api_constant.dart';
// import 'package:jualbay/widget/widget_global.dart';
import 'package:provider/provider.dart';


class SectionSlider extends StatelessWidget {
  const SectionSlider({
    Key key,
    this.imageSlide ="",
  }) : super(key: key);

// final List<String> imageUrl;
final String imageSlide;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      child: 
      CarouselSlider(
                items: [
                   Consumer<DetailProdukProvider>(
              builder: (context, provider, _) {
                  return Container(
                    height: 150,
                    child: ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemCount: provider.detailProdukModel.reslut[0].thumbsSlide.length,
                      itemBuilder: (BuildContext context, int index) {
                            //  print(provider.detailProdukModel.reslut[0].thumbsSlide[index]);               
                        return Container(
                          width: MediaQuery.of(context).size.width,
                          child: CachedNetworkImage(
                              fit: BoxFit.fitHeight, 
                              imageUrl : provider.detailProdukModel.reslut[0].thumbsSlide[index],                              
                          ),
                        );
                      },
                    ),
                  );
                }
            ), 
          
        ],
        options: CarouselOptions(
            autoPlay: false,
            enlargeCenterPage: false,
            viewportFraction: 1,
            height: 300,
            onPageChanged: (index, reason) {}),
      ),
    );
  }
} 