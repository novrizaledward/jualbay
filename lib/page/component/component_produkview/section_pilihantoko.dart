// <<<<<<< HEAD
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jualbay/dummy/flashsaleList.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/network/api_constant.dart';
import 'package:jualbay/network/api_manager.dart';
import 'package:jualbay/widget/widget_global.dart';
import 'package:jualbay/widget/widget_toko.dart';
import 'package:provider/provider.dart';
// import 'package:timeago/timeago.dart';

class SectionPilihanToko extends StatelessWidget {
  const SectionPilihanToko({
    Key key,
    this.namaToko,
    this.lokasiToko,
    this.terakhirLogin = "",
    this.fotoToko,

    this.img,
    this.stock,
    this.diskonPrice,
    this.normalPrice,
    this.finalPrice,
    this.flashSale,
    this.discountType,
    this.cobaTesla,
    this.idProduct,
    this.idProduk,
  }) : super(key: key);

  final String namaToko;
  final String lokasiToko;
  final String terakhirLogin;
  final String fotoToko;

  final String img;
  final int stock;
  final dynamic diskonPrice;
  final int normalPrice;
  final int finalPrice;
  final FlashSale flashSale;
  final dynamic discountType;
  final String cobaTesla;
  final int idProduct;
  final int idProduk;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(top: 5),
      color: Colors.white,
      child: Column(
        children: [
          Row(
            children: [
              Container(
                width: 60,
                height: 60,
                child: CachedNetworkImage(
                    imageBuilder: (context, imageProvider) => Container(
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: clrUnguDark,
                            ),
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            image: DecorationImage(
                                image: imageProvider, fit: BoxFit.cover),
                          ),
                        ),
                    // imageUrl: dummyFlashSale[0].img,
                    imageUrl: fotoToko,
                    progressIndicatorBuilder: (context, url, proggres) {
                      return Center(
                        child: LoadingWidget(),
                      );
                    }),
              ),
              SizedBox(
                width: 5,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    // "Rumah Perabot PKU",
                    namaToko,
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      color: Colors.black,
                      fontSize: 16,
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    children: [
                      SvgPicture.asset("assets/svg/productview/icon-map.svg"),
                      SizedBox(
                        width: 5,
                      ),
                      // Text("Kab. Kampar"),
                      Text(lokasiToko),
                      SizedBox(
                        width: 5,
                      ),
                      SvgPicture.asset("assets/svg/productview/icon-dot.svg"),
                      SizedBox(
                        width: 5,
                      ),
                      // Text("7 Menit Lalu")
                      // Text("7 Menit Lalu")
                      Text(
                          // jiffy(DateTime(terakhirLogin)).fromNow();
                          terakhirLogin)
                    ],
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    height: 20,
                    child: Row(
                      children: [
                        RichText(
                          text: TextSpan(children: <TextSpan>[
                            TextSpan(
                              text: "4.5",
                              style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w600,
                                color: clrMerahDark,
                              ),
                            ),
                            TextSpan(text: " "),
                            TextSpan(
                              text: "Penilaian",
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                color: Colors.black,
                                fontSize: 12,
                              ),
                            ),
                          ]),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 3),
                          child: VerticalDivider(
                            color: clrDivider,
                          ),
                        ),
                        RichText(
                          text: TextSpan(children: <TextSpan>[
                            TextSpan(
                              text: "25",
                              style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w600,
                                color: clrMerahDark,
                              ),
                            ),
                            TextSpan(text: " "),
                            TextSpan(
                              text: "Produk",
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                color: Colors.black,
                                fontSize: 12,
                              ),
                            ),
                          ]),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(vertical: 3),
                          child: VerticalDivider(
                            color: clrDivider,
                          ),
                        ),
                        RichText(
                          text: TextSpan(children: <TextSpan>[
                            TextSpan(
                              text: "95%",
                              style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w600,
                                color: clrMerahDark,
                              ),
                            ),
                            TextSpan(text: " "),
                            TextSpan(
                              text: "Respon",
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                color: Colors.black,
                                fontSize: 12,
                              ),
                            ),
                          ]),
                        ),
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
          Divider(
            color: clrDivider,
            height: 30,
          ),

         Container(
      // height: 190,
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.2),
            spreadRadius: 0.5,
            blurRadius: 5,
            offset: Offset(0, 1), // changes position of shadow
          ),
        ],
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(10)),
      ),
      child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: 
            Container(
              margin: EdgeInsets.symmetric(horizontal: 5),
              child:
              
              Row(
                children: [Consumer<DetailProdukProvider>(
                builder: (context, provider, _) {
                  if (provider.detailProdukModel== null) {
                    provider.fetchDetailProduk();
                    return CircularProgressIndicator();
                  } else { 
                    return            Container(
                      height: 175,
                      child: ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemCount: provider.detailProdukModel.reslut[0].pilihanToko.length,
                        itemBuilder: (BuildContext context, int index) {
                          // print(provider.detailProdukModel.reslut[0].pilihanToko.length);
                          return 
                          PilihanToko( 
                            hargaDiskon : provider.detailProdukModel.reslut[0].pilihanToko[index].finalPrice, 
                            // imgProduct: provider.detailProdukModel.reslut[0].pilihanToko[index].thumbs.toString(),
                            codStatus: provider.detailProdukModel.reslut[0].pilihanToko[index].codStatus, 
                            judulProduk: provider.detailProdukModel.reslut[0].pilihanToko[index].title,  
                          );
                        },
                      ),
                    );
                  }
                },
              ),

                ]
              ) 
            ),
          
        ),
    ),
  
                  
        ],
      ),
    );
  }
}
