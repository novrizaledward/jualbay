// <<<<<<< HEAD
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/helpers/constant.dart';
import 'package:jualbay/network/api_manager.dart';
import 'package:provider/provider.dart';

// void main() {
//    var iconsLabel = {detailproduk.detailProdukModel.reslut[0].icons};
//    iconsLabel.forEach((k,v) => print('${k}: ${v}'));
// }

class SectionTitle extends StatelessWidget {
  const SectionTitle({
    Key key,
    this.namaProduk,
    this.normalPrice,
    this.hargaAkhir,
    this.namaToko,
    this.diskonStatus,
    this.discountType,
    this.jumlahDiscount,
    this.iconsLabel,
  }) : super(key: key);

  final String namaProduk;
  final String normalPrice;
  final int hargaAkhir;
  final String namaToko;
  final dynamic diskonStatus;
  final dynamic discountType;
  final dynamic jumlahDiscount;
  final dynamic iconsLabel;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(top: 5),
      color: Colors.white,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Rp. " + hargaAkhir.toString(),
                style: stylePrice,
              ),
              SvgPicture.asset("assets/svg/productview/button-favorite.svg")
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: [
               jumlahDiscount != "0"
              // diskonStatus == true
                  ? Text(
                      "Rp. " + normalPrice.toString(),
                      style: styleDiskon.copyWith(color: clrSubText),
                    )
                  : Container(),

              SizedBox(
                width: 10,
              ),
              // diskonStatus == true
              jumlahDiscount != "0"
                  ? Container(
                      padding: EdgeInsets.all(3),
                      decoration: BoxDecoration(
                        color: clrMerahDark,
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                      ),
                      child: discountType.toString() == "percent"
                          ? Text(
                              jumlahDiscount + " %",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600),
                            )
                          : Text(
                              "Rp " + jumlahDiscount,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w600),
                            ))
                  :
                  // discountType == false ?
                  Container(),
              // : Container(),
              //batas aman

               Consumer<DetailProdukProvider>(
              builder: (context, provider, _) {
                  return Container(
                    height: 14,
                    
                    child: ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemCount: provider.detailProdukModel.reslut[0].icons.length,
                      itemBuilder: (BuildContext context, int index) {
                             print(provider.detailProdukModel.reslut[0].icons[index]);               
                        return Container(
                          // width: MediaQuery.of(context).size.width,
                          width: 38,
                          child: CachedNetworkImage(
                              fit: BoxFit.fitWidth, 
                              imageUrl : provider.detailProdukModel.reslut[0].icons[index],                              
                          ),
                        );
                      },
                    ),
                  );
                }
            ), 
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            // "Blender USB Terbaik Pilihan Gold Premium Diamond Oke Punya USB Blender Pilihan Tepanjang yang terbaik dari yang pernah ada segalanya ",
            namaProduk,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            style: styleTitle,
          ),
          SizedBox(
            height: 5,
          ),
          Container(
            height: 20,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                RatingBar.builder(
                  initialRating: 4.5,
                  minRating: 1,
                  itemSize: 15,
                  maxRating: 10,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 5,
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  onRatingUpdate: (rating) {
                    print(rating);
                  },
                ),
                Text(
                  "4.5",
                  style: TextStyle(
                    fontWeight: FontWeight.w800,
                    fontSize: 16,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 3),
                  child: VerticalDivider(
                    color: clrDivider,
                    width: 15,
                  ),
                ),
                Text(
                  "12 Diskusi",
                  style: TextStyle(
                    fontSize: 15,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 3),
                  child: VerticalDivider(
                    color: clrDivider,
                    width: 15,
                  ),
                ),
                Text(
                  "371 Terjual",
                  style: TextStyle(
                    fontSize: 15,
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: clrDivider,
            height: 15,
          ),
          Row(
            children: [
              SvgPicture.asset("assets/svg/productview/icon-home.svg"),
              SizedBox(
                width: 5,
              ),
              Text(
                // "Rumah Perabot PKU",
                namaToko,
                style: TextStyle(
                  color: clrSubText,
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
