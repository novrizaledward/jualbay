import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/network/api_constant.dart';
import 'package:jualbay/widget/widget_global.dart';
import 'package:provider/provider.dart';
import 'package:jualbay/network/api_manager.dart';

class SectionUntukKamu extends StatelessWidget {
  const SectionUntukKamu({
    Key key,
    this.resus,
  }) : super(key: key);

  final String resus;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(top: 5),
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          TitleOfSection(
            title: "UNTUK KAMU",
            insertRow: false,
          ),
          SizedBox(
            height: 10,
          ),
          Flexible(
              fit: FlexFit.loose,
              child: Container(child: Consumer<DetailProdukProvider>(
                  builder: (context, provider, _) {
                provider.fetchDetailProduk();
                if (provider.detailProdukModel == null) {
                  return CircularProgressIndicator();
                } else {
                  return GridView.builder(
                    primary: false,
                    // itemCount: 10,
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 15,
                      childAspectRatio: 0.64,
                    ),
                    scrollDirection: Axis.vertical,
                    itemCount:
                        provider.detailProdukModel.reslut[0].rekom.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black.withOpacity(0.3),
                              spreadRadius: 0.1,
                              blurRadius: 2,
                              offset:
                                  Offset(0, 1), // changes position of shadow
                            ),
                          ],
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10)),
                        ),
                        child: Column(
                          children: [
                            Stack(
                              children: [
                                CachedNetworkImage(
                                  imageUrl:
                                      // "https://id-test-11.slatic.net/original/68ae5dbfe3526ecdffc416065fb751c7.jpg_340x340q80.jpg",
                                      ApiConstant.baseUrl +
                                          provider.detailProdukModel.reslut[0]
                                              .rekom[index].thumbs,
                                  imageBuilder: (context, imageProvider) =>
                                      Container(
                                    height: 80,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          topRight: Radius.circular(10)),
                                      image: DecorationImage(
                                          image: imageProvider,
                                          fit: BoxFit.fill),
                                    ),
                                  ),
                                ),
                                provider.detailProdukModel.reslut[0]
                                            .rekom[index].discountStatus ==
                                        true
                                    ? Positioned(
                                        top: 0,
                                        right: 0,
                                        child: Container(
                                          height: 15,
                                          // width: 25,
                                          decoration: BoxDecoration(
                                            color: clrUnguDark,
                                            borderRadius: BorderRadius.only(
                                                bottomLeft: Radius.circular(10),
                                                topRight: Radius.circular(10)),
                                          ),
                                          child: Center(
                                            child: provider
                                                        .detailProdukModel
                                                        .reslut[0]
                                                        .rekom[index]
                                                        .discountType ==
                                                    "percent"
                                                ? Text(
                                                    provider
                                                            .detailProdukModel
                                                            .reslut[0]
                                                            .rekom[index]
                                                            .discount
                                                            .toString() +
                                                        "%",
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 9,
                                                    ),
                                                  )
                                                : Text(
                                                    "Rp. " +
                                                        provider
                                                            .detailProdukModel
                                                            .reslut[0]
                                                            .rekom[index]
                                                            .discount
                                                            .toString(),
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 9,
                                                    ),
                                                  ),
                                          ),
                                        ),
                                      )
                                    : Container(),
                                provider.detailProdukModel.reslut[0]
                                            .rekom[index].cod ==
                                        true
                                    ? Positioned(
                                        bottom: 0,
                                        left: 0,
                                        child: SvgPicture.asset(
                                            "assets/svg/icon-cod.svg"),
                                      )
                                    : Container()
                                // Positioned(
                                //   height: 20,
                                //     width: 35,
                                //   bottom: 0,
                                //   left: 0,
                                //   child: SvgPicture.asset(
                                //       "assets/svg/icon-codlarge.svg"),
                                // )
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.all(5),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    child: Text(
                                      // "Original Top Blues Terbaik Pilihan Gold Premium Dia masih terbaik dalam dunia mungkin akan menjadi  ",
                                      provider.detailProdukModel.reslut[0]
                                          .rekom[index].title,
                                      // resus,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 2,
                                      style: TextStyle(
                                        fontSize: 10,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Container(
                                    child: Text(
                                      // "Rp. 10.000.000",
                                      "Rp. " +
                                          provider.detailProdukModel.reslut[0]
                                              .rekom[index].finalPrice
                                              .toString(),
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        fontSize: 12,
                                        color: clrUnguDark,
                                        fontWeight: FontWeight.w800,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    child: Text(
                                      // "Rp. 75.000",
                                      "Rp. " +
                                          provider.detailProdukModel.reslut[0]
                                              .rekom[index].salePrice
                                              .toString(),
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 2,
                                      style: TextStyle(
                                          fontSize: 14,
                                          decoration:
                                              TextDecoration.lineThrough,
                                          color: clrSubText,
                                          fontWeight: FontWeight.w800),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          SvgPicture.asset(
                                              "assets/svg/productview/icon-home.svg"),
                                          Text(
                                            // "C2K Olshop",
                                            provider.detailProdukModel.reslut[0]
                                                .rekom[index].displayName,
                                            style: TextStyle(
                                              fontSize: 8,
                                              color: Colors.black,
                                            ),
                                          )
                                        ],
                                      ),
                                      SvgPicture.asset(
                                          "assets/svg/productview/button-more.svg")
                                    ],
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      );
                    },
                  );
                }
              }))),
          SizedBox(
            height: 5,
          ),
        ],
      ),
    );
  }
}
