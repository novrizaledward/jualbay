// <<<<<<< HEAD
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jualbay/widget/widget_global.dart';
import 'package:jualbay/page/component/component_produkview/section_produkviewkurir.dart/section_produkviewkurir.dart';

class SectionKurir extends StatelessWidget {
  const SectionKurir({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
     void slideSheet() {
      showModalBottomSheet(
          context: context,
          builder: (context) {
            return ProdukViewKurir();
          });
    }
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(top: 5),
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TitleOfSection(
                title: "KURIR",
                insertRow: false,
              ),
              SizedBox(
                height: 5,
              ),
              Text(
                "Ongkos Kirim Rp. 35.000",
                style: TextStyle(),
              )
            ],
          ),
          InkWell(
            onTap: slideSheet,
            child: Padding(
              padding: EdgeInsets.only(right: 10),
              child:
                  SvgPicture.asset("assets/svg/productview/icon-arrownext.svg"),
            ),
          )
        ],
      ),
    );
  }
}