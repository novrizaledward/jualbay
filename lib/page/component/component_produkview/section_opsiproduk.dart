// <<<<<<< HEAD
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jualbay/dummy/flashsaleList.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/page/component/component_produkview/section_produkviewvariant/section_produkviewvariant.dart';
import 'package:jualbay/widget/widget_global.dart';

class SectionOpsiProduk extends StatefulWidget {
  const SectionOpsiProduk({
    Key key,
  }) : super(key: key);

  @override
  _SectionOpsiProdukState createState() => _SectionOpsiProdukState();
}

class _SectionOpsiProdukState extends State<SectionOpsiProduk> {
  final Color colors = clrBorder;
    void slideVariant() {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return ProdukViewVariant();
        });
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 150,
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(top: 5),
      color: Colors.white,
      child: Column(
        children: [
          TitleOfSection(
            title: "OPSI PRODUK",
            insertRow: false,
          ),
          Container(
            height: 80,
            child: Row(
              children: [
                Expanded(
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: 4,
                    itemBuilder: (BuildContext context, int index) {
                      return InkWell(
                        onTap: () {},
                        child: Container(
                          margin:
                              EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                          width: 65,
                          height: 65,
                          child: CachedNetworkImage(
                              imageBuilder: (context, imageProvider) =>
                                  Container(
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                        color: clrDivider,
                                      ),
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5)),
                                      image: DecorationImage(
                                          image: imageProvider,
                                          fit: BoxFit.cover),
                                    ),
                                  ),
                              imageUrl: dummyFlashSale[index].img,
                              progressIndicatorBuilder:
                                  (context, url, proggres) {
                                return Center(
                                  child: LoadingWidget(),
                                );
                              }),
                        ),
                      );
                    },
                  ),
                ),
                InkWell(
                  onTap: slideVariant,
                  child: Padding(
                    padding: EdgeInsets.only(right: 10),
                    child: SvgPicture.asset(
                        "assets/svg/productview/icon-arrownext.svg"),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}