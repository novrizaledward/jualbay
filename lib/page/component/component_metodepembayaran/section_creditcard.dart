import 'package:jualbay/widget/widget_global.dart';
import 'package:flutter/material.dart';
import 'package:jualbay/helpers/colors.dart';

import 'package:jualbay/helpers/colors.dart';

class SectionCreditCard extends StatefulWidget {
  const SectionCreditCard({
    Key key,
  }) : super(key: key);

  @override
  _SectionCreditCardState createState() => _SectionCreditCardState();
}

class _SectionCreditCardState extends State<SectionCreditCard> {
  bool expandFlag = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              border: Border(
                left:
                    BorderSide(color: clrMerahDark, width: expandFlag ? 5 : 0),
                top: BorderSide(color: clrDivider),
                bottom: BorderSide(color: clrDivider),
              ),
              color: Colors.white,
            ),
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  "Credit Card",
                  style: TextStyle(color: Colors.black, fontSize: 16),
                ),
                IconButton(
                    icon: Icon(
                      expandFlag
                          ? Icons.keyboard_arrow_up
                          : Icons.keyboard_arrow_down,
                      color: clrSubText,
                      size: 30.0,
                    ),
                    onPressed: () {
                      setState(() {
                        expandFlag = !expandFlag;
                      });
                    }),
              ],
            ),
          ),
          ExpandableContainer(
            expanded: expandFlag,
            expandedHeight: 100,
            child: Container(
              color: Colors.white,
            ),
          )
        ],
      ),
    );
  }
}
