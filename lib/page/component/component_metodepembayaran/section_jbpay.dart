import 'package:jualbay/widget/widget_global.dart';
import 'package:flutter/material.dart';
import 'package:jualbay/helpers/colors.dart';

class SectionJbPay extends StatelessWidget {
  const SectionJbPay({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(color: clrDivider),
          bottom: BorderSide(color: clrDivider),
        ),
        color: Colors.white,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Image.asset("assets/svg/metodepembayaran/jbpay.png"),
              SizedBox(
                width: 10,
              ),
              Text(
                "JB-Pay",
                style: TextStyle(color: Colors.black, fontSize: 16),
              ),
            ],
          ),
          PrimaryButton(
            title: 'Isi Saldo',
            onPress: () {},
            titleColor: clrMerahDark,
            height: 35,
          )
        ],
      ),
    );
  }
}
