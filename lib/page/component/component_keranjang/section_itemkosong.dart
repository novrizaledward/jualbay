import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/widget/widget_global.dart';

class ItemKeranjangKosong extends StatelessWidget {
  const ItemKeranjangKosong({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      // height: 350,
      child: Center(
        child: Column(
          children: [
            SizedBox(
              height: 50,
            ),
            SvgPicture.asset("assets/svg/keranjangpage/shoppingcart-big.svg"),
            SizedBox(
              height: 40,
            ),
            Text(
              "Keranjang Kamu Masih Kosong",
              style: TextStyle(color: clrSubText, fontSize: 18),
            ),
            SizedBox(
              height: 20,
            ),
            PrimaryButton(
              title: "Ayo Belanja",
              onPress: () {},
              color: clrMerahDark,
              width: MediaQuery.of(context).size.width,
              marginhorizontal: 20,
            ),
            SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
