import 'package:cached_network_image/cached_network_image.dart';
import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jualbay/dummy/flashsaleList.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/helpers/constant.dart';
import 'package:jualbay/widget/widget_global.dart';

class ItemKeranjangPerToko extends StatefulWidget {
  const ItemKeranjangPerToko({
    Key key,
  }) : super(key: key);

  @override
  _ItemKeranjangPerTokoState createState() => _ItemKeranjangPerTokoState();
}

class _ItemKeranjangPerTokoState extends State<ItemKeranjangPerToko> {
  bool _value = false;
  @override
  Widget build(BuildContext context) {
    return
        // kalau itme kosong pakai widget ini !!
        // ItemKeranjangKosong();

// kalau item ada pakai widget ini
        ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: 3,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.only(top: 5),
          color: Colors.white,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      Container(
                        width: 35,
                        height: 35,
                        child: FittedBox(
                          child: CircularCheckBox(
                            value: _value,
                            onChanged: (index) {
                              setState(() {
                                _value = !_value;
                              });
                            },
                            activeColor: clrMerahDark,
                          ),
                        ),
                      ),
                      SvgPicture.asset("assets/svg/productview/icon-home.svg"),
                      SizedBox(
                        width: 5,
                      ),
                      Text(
                        "C2K Olshop",
                        style: TextStyle(
                          color: clrSubText,
                        ),
                      )
                    ],
                  ),
                  Text("Jakarta Selatan")
                ],
              ),
              Divider(
                color: clrDivider,
              ),
              ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: 2,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    child: Row(
                      children: [
                        Container(
                          width: 35,
                          height: 35,
                          child: FittedBox(
                            child: CircularCheckBox(
                              value: _value,
                              onChanged: (index) {
                                setState(() {
                                  _value = !_value;
                                });
                              },
                              activeColor: clrMerahDark,
                            ),
                          ),
                        ),
                        Container(
                          width: 65,
                          height: 65,
                          child: CachedNetworkImage(
                              imageBuilder: (context, imageProvider) =>
                                  Container(
                                    decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(5)),
                                      image: DecorationImage(
                                          image: imageProvider,
                                          fit: BoxFit.cover),
                                    ),
                                  ),
                              imageUrl: dummyFlashSale[0].img,
                              progressIndicatorBuilder:
                                  (context, url, proggres) {
                                return Center(
                                  child: LoadingWidget(),
                                );
                              }),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "Pariatur nisi veniam reprehenderit anim occaecat magna cillum dolore in cillum ipsum.",
                                maxLines: 2,
                                overflow: TextOverflow.ellipsis,
                                style: styleTitle.copyWith(fontSize: 16),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Text(
                                "Rp. 10.000.000",
                                style: stylePrice.copyWith(
                                    color: clrUnguDark, fontSize: 18),
                              ),
                              SizedBox(
                                height: 3,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Image.asset(
                                          "assets/img/productview/logo-freeongkir.png"),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Image.asset(
                                          "assets/img/productview/logo-cod.png"),
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Container(
                                          child: SvgPicture.asset(
                                              "assets/svg/keranjangpage/minus-item.svg")),
                                      SizedBox(
                                        width: 3,
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text("0"),
                                          Container(
                                            width: 40,
                                            child: Divider(
                                              color: clrDivider,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        width: 3,
                                      ),
                                      Container(
                                          child: SvgPicture.asset(
                                              "assets/svg/keranjangpage/plus-item.svg")),
                                    ],
                                  ),
                                  SvgPicture.asset(
                                      "assets/svg/keranjangpage/delete.svg")
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  );
                },
              ),
            ],
          ),
        );
      },
    );
  }
}
