// <<<<<<< HEAD
import 'package:flutter/material.dart';
import 'package:jualbay/widget/widget_global.dart';

class MenuProfilAkun extends StatelessWidget {
  const MenuProfilAkun({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      padding: EdgeInsets.fromLTRB(10, 10, 10, 20),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 3,
              offset: Offset(0, 1), // changes position of shadow
            ),
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "AKUN",
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
          CostumWgtList(
            svg: "assets/svg/profilpage/menu-akun.svg",
            svgUse: true,
            title: "Menu Akun 1",
            onTap: () {},
          ),
          CostumWgtList(
            svg: "assets/svg/profilpage/menu-akun.svg",
            svgUse: true,
            title: "Menu Akun 2",
            onTap: () {},
          ),
          CostumWgtList(
            svg: "assets/svg/profilpage/menu-akun.svg",
            svgUse: true,
            title: "Menu Akun 3",
            onTap: () {},
          ),
        ],
      ),
    );
  }
}
// =======
// import 'package:flutter/material.dart';
// import 'package:jualbay/widget/widget_global.dart';

// class MenuProfilAkun extends StatelessWidget {
//   const MenuProfilAkun({
//     Key key,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
//       padding: EdgeInsets.fromLTRB(10, 10, 10, 20),
//       decoration: BoxDecoration(
//           borderRadius: BorderRadius.all(Radius.circular(10)),
//           color: Colors.white,
//           boxShadow: [
//             BoxShadow(
//               color: Colors.black.withOpacity(0.5),
//               spreadRadius: 1,
//               blurRadius: 3,
//               offset: Offset(0, 1), // changes position of shadow
//             ),
//           ]),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: [
//               Text(
//                 "AKUN",
//                 style: TextStyle(
//                   color: Colors.black,
//                   fontWeight: FontWeight.w600,
//                 ),
//               ),
//             ],
//           ),
//           CostumWgtList(
//             svg: "assets/svg/profilpage/menu-akun.svg",
//             svgUse: true,
//             title: "Menu Akun 1",
//             onTap: () {},
//           ),
//           CostumWgtList(
//             svg: "assets/svg/profilpage/menu-akun.svg",
//             svgUse: true,
//             title: "Menu Akun 2",
//             onTap: () {},
//           ),
//           CostumWgtList(
//             svg: "assets/svg/profilpage/menu-akun.svg",
//             svgUse: true,
//             title: "Menu Akun 3",
//             onTap: () {},
//           ),
//         ],
//       ),
//     );
//   }
// }
// >>>>>>> 625fb91f5671b4fae18b820fec00ec47d18cf23d
