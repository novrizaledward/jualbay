import 'package:flutter/material.dart';
import 'package:jualbay/widget/widget_global.dart';

class MenuProfilInfoLainnya extends StatelessWidget {
  const MenuProfilInfoLainnya({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      padding: EdgeInsets.fromLTRB(10, 10, 10, 20),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 3,
              offset: Offset(0, 1), // changes position of shadow
            ),
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text( 
                "INFO LAINNYA",
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ],
          ),
          CostumWgtList(
            icon: Icons.note,
            title: "Kebijakan Privasi",
            onTap: () {},
          ),
          CostumWgtList(
            icon: Icons.note,
            title: "Ketentuan Layanan",
            onTap: () {},
          ),
          CostumWgtList(
            svgUse: true,
            svg: "assets/svg/profilpage/faq.svg",
            title: "Bantuan",
            onTap: () {},
          ),
        ],
      ),
    );
  }
}