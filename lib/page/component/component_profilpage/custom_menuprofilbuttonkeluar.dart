// <<<<<<< HEAD
import 'package:flutter/material.dart';
import 'package:jualbay/helpers/colors.dart';

class MenuProfilButtonKeluar extends StatelessWidget {
  const MenuProfilButtonKeluar({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      child: FlatButton(
        onPressed: () {},
        child: Row(
          children: [
            Icon(
              Icons.close,
              color: clrMerahDark,
            ),
            Expanded(
              child: Center(
                child: Text(
                  "Keluar",
                  style: TextStyle(
                    color: clrMerahDark,
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
          ],
        ),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(7),
            side: BorderSide(color: clrMerahDark)),
      ),
    );
  }
}
// =======
// import 'package:flutter/material.dart';
// import 'package:jualbay/helpers/colors.dart';

// class MenuProfilButtonKeluar extends StatelessWidget {
//   const MenuProfilButtonKeluar({
//     Key key,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       height: 45,
//       width: MediaQuery.of(context).size.width,
//       margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
//       child: FlatButton(
//         onPressed: () {},
//         child: Row(
//           children: [
//             Icon(
//               Icons.close,
//               color: clrMerahDark,
//             ),
//             Expanded(
//               child: Center(
//                 child: Text(
//                   "Keluar",
//                   style: TextStyle(
//                     color: clrMerahDark,
//                     fontSize: 18,
//                     fontWeight: FontWeight.w600,
//                   ),
//                 ),
//               ),
//             ),
//           ],
//         ),
//         shape: RoundedRectangleBorder(
//             borderRadius: BorderRadius.circular(7),
//             side: BorderSide(color: clrMerahDark)),
//       ),
//     );
//   }
// }
// >>>>>>> 625fb91f5671b4fae18b820fec00ec47d18cf23d
