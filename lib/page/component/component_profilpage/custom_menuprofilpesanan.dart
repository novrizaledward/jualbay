// <<<<<<< HEAD
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MenuProfilPesanan extends StatelessWidget {
  const MenuProfilPesanan({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.5),
              spreadRadius: 1,
              blurRadius: 3,
              offset: Offset(0, 1), // changes position of shadow
            ),
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "PESANAN",
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w600,
                ),
              ),
              Text("Lihat Riwayat")
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Column(
                children: [
                  SvgPicture.asset("assets/svg/profilpage/belumbayar.svg"),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    "Belum Bayar",
                    style: TextStyle(),
                  )
                ],
              ),
              Column(
                children: [
                  SvgPicture.asset("assets/svg/profilpage/dikemas.svg"),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    "Dikemas",
                    style: TextStyle(),
                  )
                ],
              ),
              Column(
                children: [
                  SvgPicture.asset("assets/svg/profilpage/dikirim.svg"),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    "Dikirim",
                    style: TextStyle(),
                  )
                ],
              ),
              Column(
                children: [
                  SvgPicture.asset("assets/svg/profilpage/penilaian.svg"),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    "Penilaian",
                    style: TextStyle(),
                  )
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}
// =======
// import 'package:flutter/material.dart';
// import 'package:flutter_svg/flutter_svg.dart';

// class MenuProfilPesanan extends StatelessWidget {
//   const MenuProfilPesanan({
//     Key key,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       height: 100,
//       margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
//       padding: EdgeInsets.all(10),
//       decoration: BoxDecoration(
//           borderRadius: BorderRadius.all(Radius.circular(10)),
//           color: Colors.white,
//           boxShadow: [
//             BoxShadow(
//               color: Colors.black.withOpacity(0.5),
//               spreadRadius: 1,
//               blurRadius: 3,
//               offset: Offset(0, 1), // changes position of shadow
//             ),
//           ]),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         children: [
//           Row(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: [
//               Text(
//                 "PESANAN",
//                 style: TextStyle(
//                   color: Colors.black,
//                   fontWeight: FontWeight.w600,
//                 ),
//               ),
//               Text("Lihat Riwayat")
//             ],
//           ),
//           Row(
//             mainAxisAlignment: MainAxisAlignment.spaceAround,
//             crossAxisAlignment: CrossAxisAlignment.end,
//             children: [
//               Column(
//                 children: [
//                   SvgPicture.asset("assets/svg/profilpage/belumbayar.svg"),
//                   SizedBox(
//                     height: 5,
//                   ),
//                   Text(
//                     "Belum Bayar",
//                     style: TextStyle(),
//                   )
//                 ],
//               ),
//               Column(
//                 children: [
//                   SvgPicture.asset("assets/svg/profilpage/dikemas.svg"),
//                   SizedBox(
//                     height: 5,
//                   ),
//                   Text(
//                     "Dikemas",
//                     style: TextStyle(),
//                   )
//                 ],
//               ),
//               Column(
//                 children: [
//                   SvgPicture.asset("assets/svg/profilpage/dikirim.svg"),
//                   SizedBox(
//                     height: 5,
//                   ),
//                   Text(
//                     "Dikirim",
//                     style: TextStyle(),
//                   )
//                 ],
//               ),
//               Column(
//                 children: [
//                   SvgPicture.asset("assets/svg/profilpage/penilaian.svg"),
//                   SizedBox(
//                     height: 5,
//                   ),
//                   Text(
//                     "Penilaian",
//                     style: TextStyle(),
//                   )
//                 ],
//               ),
//             ],
//           )
//         ],
//       ),
//     );
//   }
// }
// >>>>>>> 625fb91f5671b4fae18b820fec00ec47d18cf23d
