import 'package:flutter/material.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/helpers/constant.dart';
import 'package:jualbay/widget/widget_global.dart';

class SectionTotalTagihan extends StatelessWidget {
  const SectionTotalTagihan({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 5),
      padding: EdgeInsets.all(10),
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Total Tagihan"),
              Text(
                "Rp. 20.000.000",
                style: stylePrice.copyWith(color: clrUnguDark),
              ),
            ],
          ),
          PrimaryButton(
            color: clrMerahDark,
            title: 'Buat Pesanan',
            onPress: () {},
            marginhorizontal: 10,
          )
        ],
      ),
    );
  }
}
