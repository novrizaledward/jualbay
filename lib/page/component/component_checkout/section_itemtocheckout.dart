import 'package:flutter/material.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/helpers/constant.dart';
import 'package:jualbay/page/component/component_checkout/list_item/list_item.dart';
import 'package:jualbay/page/pengirimanpage.dart';

class SectionItemToCheckout extends StatelessWidget {
  const SectionItemToCheckout({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      margin: EdgeInsets.only(top: 5),
      child: Column(
        children: [
          ListItemToCheckout(),
          Divider(
            color: clrDivider,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "Pilih Pengiriman",
                  style: TextStyle(fontWeight: FontWeight.w700, fontSize: 16),
                ),
                IconButton(
                    padding: EdgeInsets.symmetric(vertical: 0),
                    icon: Icon(
                      Icons.arrow_forward_ios,
                      color: Colors.black.withOpacity(0.5),
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PengirimanPage()));
                    })
              ],
            ),
          ),
          Divider(
            color: clrDivider,
          ),
          Container(
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("Subtotal (5 Produk)"),
                Text(
                  "Rp.10.000.000",
                  style: stylePrice.copyWith(color: clrMerahDark, fontSize: 18),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
