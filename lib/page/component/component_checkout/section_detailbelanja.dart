import 'package:flutter/material.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/widget/widget_global.dart';

class SectionDetailBelanja extends StatelessWidget {
  const SectionDetailBelanja({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 5),
      padding: EdgeInsets.all(10),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Detail Belanja",
            style: TextStyle(fontWeight: FontWeight.w700, fontSize: 16),
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Total Belanja"),
              Text("Rp. 20.000.000"),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Total Pengiriman"),
              Text("Rp. 35.000"),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Biaya Penanganan"),
              Text("Rp. 0"),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          MySeparator(
            color: clrSubText,
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Total Pembayaran",
                style: TextStyle(fontWeight: FontWeight.w700, fontSize: 16),
              ),
              Text("Rp. 10.000.000"),
            ],
          ),
        ],
      ),
    );
  }
}
