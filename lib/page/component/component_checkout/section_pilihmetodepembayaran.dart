import 'package:flutter/material.dart';
import 'package:jualbay/page/metodepembayaranpage.dart';

class SectionPilihMetodePembayaran extends StatelessWidget {
  const SectionPilihMetodePembayaran({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(top: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Pilih Metode Pembayaran",
            style: TextStyle(fontWeight: FontWeight.w700, fontSize: 16),
          ),
          IconButton(
              padding: EdgeInsets.symmetric(vertical: 0),
              icon: Icon(
                Icons.arrow_forward_ios,
                color: Colors.black.withOpacity(0.5),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => MetodePembayaran()));
              })
        ],
      ),
    );
  }
}
