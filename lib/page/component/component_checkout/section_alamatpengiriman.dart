import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jualbay/helpers/colors.dart';

class SectionAlamatPengiriman extends StatelessWidget {
  const SectionAlamatPengiriman({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      color: Colors.white,
      child: Column(
        children: [
          Row(
            children: [
              SvgPicture.asset(
                "assets/svg/productview/icon-map.svg",
                color: clrMerahDark,
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                "Alamat Pengiriman",
                style: TextStyle(fontWeight: FontWeight.w500),
              )
            ],
          ),
          SizedBox(
            height: 5,
          ),
          Row(
            children: [
              Expanded(
                child: Text(
                  "Perkantoran Grand Sudirman Blok D 18, Jl. Datuk Setia Maharaja, Kel. Tangkerang Selatan, Kec. Bukit Raya, Kota Pekanbaru - Riau, Indonesia 28288",
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
              ),
              IconButton(
                  icon: Icon(
                    Icons.arrow_forward_ios,
                    color: Colors.black.withOpacity(0.5),
                  ),
                  onPressed: () {})
            ],
          )
        ],
      ),
    );
  }
}
