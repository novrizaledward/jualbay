import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jualbay/dummy/flashsaleList.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/helpers/constant.dart';
import 'package:jualbay/widget/widget_global.dart';

class ListItemToCheckout extends StatelessWidget {
  const ListItemToCheckout({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      color: Colors.white,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  SvgPicture.asset("assets/svg/productview/icon-home.svg"),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    "C2K Olshop",
                    style: TextStyle(fontWeight: FontWeight.w500),
                  )
                ],
              ),
              Text("Jakarta Selatan")
            ],
          ),
          Divider(
            color: clrDivider,
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            child: Row(
              children: [
                Container(
                  width: 65,
                  height: 65,
                  child: CachedNetworkImage(
                      imageBuilder: (context, imageProvider) => Container(
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              image: DecorationImage(
                                  image: imageProvider, fit: BoxFit.cover),
                            ),
                          ),
                      imageUrl: dummyFlashSale[0].img,
                      progressIndicatorBuilder: (context, url, proggres) {
                        return Center(
                          child: LoadingWidget(),
                        );
                      }),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Pariatur nisi veniam reprehenderit anim occaecat magna cillum dolore in cillum ipsum.",
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: styleTitle.copyWith(fontSize: 16),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Rp. 10.000.000",
                            style: stylePrice.copyWith(
                                color: clrUnguDark, fontSize: 18),
                          ),
                          Text("Qty: 5 (500 gr)")
                        ],
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Image.asset(
                              "assets/img/productview/logo-freeongkir.png"),
                          SizedBox(
                            width: 10,
                          ),
                          Image.asset("assets/img/productview/logo-cod.png"),
                        ],
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
          TextField(
            maxLines: 1,
            style: TextStyle(
                fontSize: 16, color: Colors.black, fontWeight: FontWeight.w400),
            decoration: InputDecoration(
              focusColor: clrMerahDark,
              labelStyle: TextStyle(color: clrSubText),
              labelText: 'Catatan (opsional)',
              isDense: false,
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: clrMerahDark),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
