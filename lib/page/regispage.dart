// <<<<<<< HEAD
import 'package:flutter/material.dart';
import 'package:jualbay/helpers/colors.dart';

class RegisPage extends StatefulWidget {
  RegisPage({Key key}) : super(key: key);

  @override
  _RegisPageState createState() => _RegisPageState();
}

class _RegisPageState extends State<RegisPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
          color: Colors.black,
        ),
        title: Text(
          "Daftar",
          style: TextStyle(color: Colors.black),
        ),
        actions: [
          FlatButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text(
              "Login",
              style: TextStyle(
                color: clrMerahDark,
                fontSize: 18,
              ),
            ),
          ),
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              SizedBox(
                height: 30,
              ),
              Image.asset("assets/img/logo.png"),
              SizedBox(
                height: 20,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  maxLines: 1,
                  enableSuggestions: true,
                  autocorrect: false,
                  onChanged: (value) {},
                  style: TextStyle(
                      fontSize: 16,
                      height: 1,
                      color: Colors.black,
                      fontWeight: FontWeight.w400),
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(top: 18),
                    hintText: "Email",
                    prefixIcon: Icon(Icons.person),
                    isDense: false,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  maxLines: 1,
                  enableSuggestions: true,
                  autocorrect: false,
                  onChanged: (value) {},
                  style: TextStyle(
                      fontSize: 16,
                      height: 1,
                      color: Colors.black,
                      fontWeight: FontWeight.w400),
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(top: 18),
                    hintText: "Nomor Ponsel",
                    prefixIcon: Icon(Icons.phone),
                    isDense: false,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                    maxLines: 1,
                    obscureText: true,
                    onChanged: (value) {},
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                        fontWeight: FontWeight.w400),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(top: 18),
                      hintText: "Password",
                      prefixIcon: Icon(Icons.lock),
                      suffixIcon: Icon(Icons.visibility),
                      isDense: false,
                    )),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                    maxLines: 1,
                    obscureText: true,
                    onChanged: (value) {},
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                        fontWeight: FontWeight.w400),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(top: 18),
                      hintText: "Konfirmasi Password",
                      prefixIcon: Icon(Icons.lock),
                      suffixIcon: Icon(Icons.visibility),
                      isDense: false,
                    )),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                height: 40,
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.symmetric(horizontal: 20),
                child: FlatButton(
                  onPressed: () {},
                  child: Text(
                    "Daftar",
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  ),
                  color: clrMerahDark,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7),
                      side: BorderSide(color: Colors.red)),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Row(
                  children: [
                    Expanded(
                      child: Divider(
                        color: clrDivider,
                        height: 3,
                        thickness: 2,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      child: Text(
                        "atau masuk dengan",
                        style: TextStyle(color: clrSubText),
                      ),
                    ),
                    Expanded(
                      child: Divider(
                        color: clrDivider,
                        height: 3,
                        thickness: 2,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: 40,
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: FlatButton(
                  onPressed: () {},
                  child: Row(
                    children: [
                      Image.asset("assets/img/google-logo.png"),
                      Expanded(
                        child: Center(
                          child: Text(
                            "Google",
                            style: TextStyle(color: Colors.black, fontSize: 18),
                          ),
                        ),
                      ),
                    ],
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7),
                      side: BorderSide(color: Colors.black)),
                ),
              ),
              Container(
                height: 40,
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.symmetric(
                  horizontal: 20,
                ),
                child: FlatButton(
                  onPressed: () {},
                  child: Row(
                    children: [
                      Image.asset("assets/img/facebook-logo.png"),
                      Expanded(
                        child: Center(
                          child: Text(
                            "Facebook",
                            style: TextStyle(color: Colors.black, fontSize: 18),
                          ),
                        ),
                      ),
                    ],
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7),
                      side: BorderSide(color: Colors.black)),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Belum Punya Akun? ",
                      style: TextStyle(
                        color: clrSubText,
                      ),
                    ),
                    Text(
                      "Masuk",
                      style: TextStyle(
                        color: clrMerahDark,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Butuh Bantuan? ",
                  style: TextStyle(
                    color: clrSubText,
                  ),
                ),
                Text(
                  "Hubungi JualBuy Care",
                  style: TextStyle(
                    color: clrMerahDark,
                    fontWeight: FontWeight.w800,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
// =======
// import 'package:flutter/material.dart';
// import 'package:jualbay/helpers/colors.dart';

// class RegisPage extends StatefulWidget {
//   RegisPage({Key key}) : super(key: key);

//   @override
//   _RegisPageState createState() => _RegisPageState();
// }

// class _RegisPageState extends State<RegisPage> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.white,
//       resizeToAvoidBottomInset: false,
//       appBar: AppBar(
//         backgroundColor: Colors.white,
//         leading: IconButton(
//           icon: Icon(Icons.arrow_back),
//           onPressed: () {
//             Navigator.pop(context);
//           },
//           color: Colors.black,
//         ),
//         title: Text(
//           "Daftar",
//           style: TextStyle(color: Colors.black),
//         ),
//         actions: [
//           FlatButton(
//             onPressed: () {
//               Navigator.pop(context);
//             },
//             child: Text(
//               "Login",
//               style: TextStyle(
//                 color: clrMerahDark,
//                 fontSize: 18,
//               ),
//             ),
//           ),
//         ],
//       ),
//       body: Column(
//         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         children: [
//           Column(
//             children: [
//               SizedBox(
//                 height: 30,
//               ),
//               Image.asset("assets/img/logo.png"),
//               SizedBox(
//                 height: 20,
//               ),
//               Container(
//                 margin: EdgeInsets.symmetric(horizontal: 20),
//                 child: TextField(
//                   maxLines: 1,
//                   enableSuggestions: true,
//                   autocorrect: false,
//                   onChanged: (value) {},
//                   style: TextStyle(
//                       fontSize: 16,
//                       height: 1,
//                       color: Colors.black,
//                       fontWeight: FontWeight.w400),
//                   keyboardType: TextInputType.emailAddress,
//                   decoration: InputDecoration(
//                     contentPadding: EdgeInsets.only(top: 18),
//                     hintText: "Email",
//                     prefixIcon: Icon(Icons.person),
//                     isDense: false,
//                   ),
//                 ),
//               ),
//               Container(
//                 margin: EdgeInsets.symmetric(horizontal: 20),
//                 child: TextField(
//                   maxLines: 1,
//                   enableSuggestions: true,
//                   autocorrect: false,
//                   onChanged: (value) {},
//                   style: TextStyle(
//                       fontSize: 16,
//                       height: 1,
//                       color: Colors.black,
//                       fontWeight: FontWeight.w400),
//                   keyboardType: TextInputType.emailAddress,
//                   decoration: InputDecoration(
//                     contentPadding: EdgeInsets.only(top: 18),
//                     hintText: "Nomor Ponsel",
//                     prefixIcon: Icon(Icons.phone),
//                     isDense: false,
//                   ),
//                 ),
//               ),
//               Container(
//                 margin: EdgeInsets.symmetric(horizontal: 20),
//                 child: TextField(
//                     maxLines: 1,
//                     obscureText: true,
//                     onChanged: (value) {},
//                     style: TextStyle(
//                         fontSize: 16,
//                         color: Colors.black,
//                         fontWeight: FontWeight.w400),
//                     decoration: InputDecoration(
//                       contentPadding: EdgeInsets.only(top: 18),
//                       hintText: "Password",
//                       prefixIcon: Icon(Icons.lock),
//                       suffixIcon: Icon(Icons.visibility),
//                       isDense: false,
//                     )),
//               ),
//               Container(
//                 margin: EdgeInsets.symmetric(horizontal: 20),
//                 child: TextField(
//                     maxLines: 1,
//                     obscureText: true,
//                     onChanged: (value) {},
//                     style: TextStyle(
//                         fontSize: 16,
//                         color: Colors.black,
//                         fontWeight: FontWeight.w400),
//                     decoration: InputDecoration(
//                       contentPadding: EdgeInsets.only(top: 18),
//                       hintText: "Konfirmasi Password",
//                       prefixIcon: Icon(Icons.lock),
//                       suffixIcon: Icon(Icons.visibility),
//                       isDense: false,
//                     )),
//               ),
//               SizedBox(
//                 height: 20,
//               ),
//               Container(
//                 height: 40,
//                 width: MediaQuery.of(context).size.width,
//                 margin: EdgeInsets.symmetric(horizontal: 20),
//                 child: FlatButton(
//                   onPressed: () {},
//                   child: Text(
//                     "Daftar",
//                     style: TextStyle(color: Colors.white, fontSize: 18),
//                   ),
//                   color: clrMerahDark,
//                   shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(7),
//                       side: BorderSide(color: Colors.red)),
//                 ),
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               Container(
//                 margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
//                 child: Row(
//                   children: [
//                     Expanded(
//                       child: Divider(
//                         color: clrDivider,
//                         height: 3,
//                         thickness: 2,
//                       ),
//                     ),
//                     Container(
//                       margin: EdgeInsets.symmetric(horizontal: 20),
//                       child: Text(
//                         "atau masuk dengan",
//                         style: TextStyle(color: clrSubText),
//                       ),
//                     ),
//                     Expanded(
//                       child: Divider(
//                         color: clrDivider,
//                         height: 3,
//                         thickness: 2,
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//               Container(
//                 height: 40,
//                 width: MediaQuery.of(context).size.width,
//                 margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
//                 child: FlatButton(
//                   onPressed: () {},
//                   child: Row(
//                     children: [
//                       Image.asset("assets/img/google-logo.png"),
//                       Expanded(
//                         child: Center(
//                           child: Text(
//                             "Google",
//                             style: TextStyle(color: Colors.black, fontSize: 18),
//                           ),
//                         ),
//                       ),
//                     ],
//                   ),
//                   shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(7),
//                       side: BorderSide(color: Colors.black)),
//                 ),
//               ),
//               Container(
//                 height: 40,
//                 width: MediaQuery.of(context).size.width,
//                 margin: EdgeInsets.symmetric(
//                   horizontal: 20,
//                 ),
//                 child: FlatButton(
//                   onPressed: () {},
//                   child: Row(
//                     children: [
//                       Image.asset("assets/img/facebook-logo.png"),
//                       Expanded(
//                         child: Center(
//                           child: Text(
//                             "Facebook",
//                             style: TextStyle(color: Colors.black, fontSize: 18),
//                           ),
//                         ),
//                       ),
//                     ],
//                   ),
//                   shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(7),
//                       side: BorderSide(color: Colors.black)),
//                 ),
//               ),
//               Container(
//                 margin: EdgeInsets.symmetric(vertical: 10),
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: [
//                     Text(
//                       "Belum Punya Akun? ",
//                       style: TextStyle(
//                         color: clrSubText,
//                       ),
//                     ),
//                     Text(
//                       "Masuk",
//                       style: TextStyle(
//                         color: clrMerahDark,
//                         fontWeight: FontWeight.w800,
//                       ),
//                     ),
//                   ],
//                 ),
//               )
//             ],
//           ),
//           Container(
//             margin: EdgeInsets.symmetric(vertical: 10),
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: [
//                 Text(
//                   "Butuh Bantuan? ",
//                   style: TextStyle(
//                     color: clrSubText,
//                   ),
//                 ),
//                 Text(
//                   "Hubungi JualBuy Care",
//                   style: TextStyle(
//                     color: clrMerahDark,
//                     fontWeight: FontWeight.w800,
//                   ),
//                 ),
//               ],
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }
// >>>>>>> 625fb91f5671b4fae18b820fec00ec47d18cf23d
