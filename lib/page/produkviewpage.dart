// <<<<<<< HEAD
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jualbay/helpers/colors.dart';

import 'package:jualbay/network/api_manager.dart';
import 'package:provider/provider.dart';

import 'component/component_produkview/section_deskripsi.dart';
import 'component/component_produkview/section_kurir.dart';
import 'component/component_produkview/section_opsiproduk.dart';
import 'component/component_produkview/section_penilaian.dart';
import 'component/component_produkview/section_pilihantoko.dart';
import 'component/component_produkview/section_promo.dart';
import 'component/component_produkview/section_slider.dart';
import 'component/component_produkview/section_tanyaproduk.dart';
import 'component/component_produkview/section_title.dart';
import 'component/component_produkview/section_untukkamu.dart';

class ProdukView extends StatefulWidget {
  ProdukView({Key key/*, this.idDetailProduk*/}) : super(key: key);
  // final String idDetailProduk;
  @override
  _ProdukViewState createState() => _ProdukViewState();
}

class _ProdukViewState extends State<ProdukView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: clrbg,
      appBar: PreferredSize(
        preferredSize: Size(MediaQuery.of(context).size.width, 50),
        child: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: AppBar(
            // elevation: 0,
            backgroundColor: Colors.white,
            automaticallyImplyLeading: false,
            flexibleSpace: SafeArea(
              child: Row(
                children: [
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    child: Icon(
                      Icons.arrow_back,
                      color: clrIcon,
                    ),
                  ),
                  Expanded(
                    child: Container(
                        height: 40,
                        decoration: BoxDecoration(
                          border: Border.all(color: clrBorder),
                          borderRadius: BorderRadius.all(Radius.circular(50)),
                          color: Colors.white,
                        ),
                        margin: EdgeInsets.fromLTRB(0, 10, 5, 10),
                        child: TextFormField(
                          cursorColor: Colors.black,
                          style: TextStyle(
                            color: Colors.black,
                          ),
                          decoration: new InputDecoration(
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            prefixIcon: Icon(
                              Icons.search,
                              size: 20,
                              color: clrSubText,
                            ),
                            contentPadding: EdgeInsets.only(
                                left: 5, bottom: 19, top: 0, right: 5), 
                          ),
                        )),
                  ),
                  Container(
                    child: SvgPicture.asset(
                        "assets/svg/productview/button-share.svg"),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                    child: SvgPicture.asset(
                        "assets/svg/productview/button-checkout.svg"),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                    child: SvgPicture.asset(
                        "assets/svg/productview/button-menuappbar.svg"),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      body: Consumer<DetailProdukProvider>(
          builder: (context, detailproduk, widget) {
        detailproduk.fetchDetailProduk();
        if (detailproduk.detailProdukModel == null) {
          return CircularProgressIndicator();
        } else {
          return SingleChildScrollView(
            child:
             Column(
              children: [
                SectionSlider(
                              

                  ),
                SectionTitle(
                    namaProduk: detailproduk.detailProdukModel.reslut[0].title,
                    normalPrice : detailproduk.detailProdukModel.reslut[0].salePrice,
                    hargaAkhir : detailproduk.detailProdukModel.reslut[0].finalPrice,
                    namaToko: detailproduk.detailProdukModel.reslut[0].displayName, 
                    diskonStatus: detailproduk.detailProdukModel.reslut[0].discountStatus,
                    discountType: detailproduk.detailProdukModel.reslut[0].discountType,
                    jumlahDiscount: detailproduk.detailProdukModel.reslut[0].discount,
                    // iconsLabel = detailproduk.detailProdukModel.reslut[0].icons,

                ),
                SectionOpsiProduk(),
                SectionPromo(
                  // promoToko: detailproduk.detailProdukModel.reslut[0].voucher[index].code,
                ),
                SectionKurir(),
                SectionDeskripsi(
                    deskripsiProduk: detailproduk.detailProdukModel.reslut[0].description,
                    kategoriProduk: detailproduk.detailProdukModel.reslut[0].categoryName,
                    minimalPesan: detailproduk.detailProdukModel.reslut[0].purchaseMin,
                    beratProduk: detailproduk.detailProdukModel.reslut[0].weight,
                    kondisiProduk: detailproduk.detailProdukModel.reslut[0].productCondition,
                ),
                SectionPilihanToko(
                    namaToko: detailproduk.detailProdukModel.reslut[0].displayName,
                    lokasiToko: detailproduk.detailProdukModel.reslut[0].city,
                    terakhirLogin: detailproduk.detailProdukModel.reslut[0].lastLogin,
                    fotoToko: detailproduk.detailProdukModel.reslut[0].imageVendor,   

                ),
                SectionPenilaian(),
                SectionTanyaProduk(),
                SectionUntukKamu( 
                ),
              ],
            ),
            // }
            // ),
          );
        }
      }),
    );
  }
} 