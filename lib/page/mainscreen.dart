// <<<<<<< HEAD
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:jualbay/page/keranjangpage.dart';
import 'package:jualbay/page/profilepage.dart';

import '../helpers/colors.dart';
import 'homepage.dart';

class MainScreen extends StatefulWidget {
  MainScreen({Key key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  static List<Widget> _widgetOptions = <Widget>[
    HomePage(),
    Text(
      'Index 0: Home',
    ),
     KeranjangPage(),
    
    Text(
      'Index 2: School',
    ),
    ProfilPage()
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _widgetOptions.elementAt(_selectedIndex),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              "assets/svg/icon-home.svg",
              color: _selectedIndex == 0 ? clrUnguDark : null,
            ),
            title: Text('Home'),
            // label :'Home'
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              "assets/svg/icon-official.svg",
              color: _selectedIndex == 1 ? clrUnguDark : null,
            ),
            title: Text('Official'),
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              "assets/svg/icon-keranjang.svg",
              color: _selectedIndex == 2 ? clrUnguDark : null,
            ),
            title: Text('Keranjang'),
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              "assets/svg/icon-whishlist.svg",
              color: _selectedIndex == 3 ? clrUnguDark : null,
            ),
            title: Text('Wishlist'),
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              "assets/svg/icon-login.svg",
              color: _selectedIndex == 4 ? clrUnguDark : null,
            ),
            title: Text('Masuk'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: clrUnguDark,
        // selectedIconTheme: IconThemeData(color: clrUnguDark),
        type: BottomNavigationBarType.fixed,
        unselectedLabelStyle: TextStyle(color: clrSubText),
        // unselectedItemColor: clrSubText,
        onTap: _onItemTapped,
      ),
    );
  }
}
 