import 'package:flutter/material.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/page/component/component_metodepembayaran/section_gerai.dart';
import 'package:jualbay/page/component/component_metodepembayaran/section_transferbank.dart';
import 'package:jualbay/widget/widget_global.dart';

import 'component/component_metodepembayaran/section_creditcard.dart';
import 'component/component_metodepembayaran/section_jbpay.dart';

class MetodePembayaran extends StatefulWidget {
  MetodePembayaran({Key key}) : super(key: key);

  @override
  _MetodePembayaranState createState() => _MetodePembayaranState();
}

class _MetodePembayaranState extends State<MetodePembayaran> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: clrBg,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text(
          "Metode Pembayaran",
          style: TextStyle(color: Colors.black),
        ),
      ),
      floatingActionButton: PrimaryButton(
        color: clrMerahDark,
        title: 'Selesai',
        marginhorizontal: 10,
        onPress: () {},
        width: MediaQuery.of(context).size.width,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body: SingleChildScrollView(
        child: Column(
          children: [
            SectionJbPay(),
            SectionCreditCard(),
            SectionTransferBank(),
            SectionGerai(),
          ],
        ),
      ),
    );
  }
}
