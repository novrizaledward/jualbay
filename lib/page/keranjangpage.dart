import 'package:circular_check_box/circular_check_box.dart';
import 'package:flutter/material.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/helpers/constant.dart';
import 'package:jualbay/page/checkoutpage.dart';
import 'package:jualbay/page/component/component_produkview/section_untukkamu.dart';
import 'package:jualbay/widget/widget_global.dart';

import 'component/component_keranjang/section_itempertoko.dart';

class KeranjangPage extends StatefulWidget {
  KeranjangPage({Key key}) : super(key: key);

  @override
  _KeranjangPageState createState() => _KeranjangPageState();
}

class _KeranjangPageState extends State<KeranjangPage> {
  bool _value = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: clrbg,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: Icon(
          Icons.arrow_back,
          color: Colors.black,
        ),
        title: Text(
          "Keranjang (0)",
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.normal,
          ),
        ),
        actions: [
          FlatButton(
              onPressed: () {},
              child: Text(
                "Hapus",
                style: stylePrice.copyWith(color: clrMerahDark, fontSize: 16),
              ))
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            ItemKeranjangPerToko(),
            // SectionUntukKamu(),
          ],
        ),
      ),
      floatingActionButton: Container(
        height: 55,
        width: MediaQuery.of(context).size.width,
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Container(
                  width: 35,
                  height: 35,
                  child: FittedBox(
                    child: CircularCheckBox(
                      value: _value,
                      onChanged: (index) {
                        setState(() {
                          _value = !_value;
                        });
                      },
                      activeColor: clrMerahDark,
                    ),
                  ),
                ),
                Text("Semua"),
              ],
            ),
            Row(
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("Subtotal :"),
                    Text(
                      "Rp. 10.000.000",
                      style: stylePrice,
                    )
                  ],
                ),
                PrimaryButton(
                  color: clrMerahDark,
                  title: 'Checkout',
                  onPress: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => CheckOutPage()));
                  },
                  marginhorizontal: 10,
                )
              ],
            )
          ],
        ),
      ),
      floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterDocked,
    );
  }
}
