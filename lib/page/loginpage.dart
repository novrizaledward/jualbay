// <<<<<<< HEAD
import 'package:flutter/material.dart';
import 'package:jualbay/helpers/colors.dart';
import 'package:jualbay/page/mainscreen.dart';
import 'package:jualbay/page/regispage.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: Icon(
          Icons.arrow_back,
          color: Colors.black,
        ),
        title: Text(
          "Masuk",
          style: TextStyle(color: Colors.black),
        ),
        actions: [
          FlatButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => RegisPage()));
            },
            child: Text(
              "Daftar",
              style: TextStyle(
                color: clrMerahDark,
                fontSize: 18,
              ),
            ),
          ),
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              SizedBox(
                height: 60,
              ),
              Image.asset("assets/img/logo.png"),
              SizedBox(
                height: 50,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                  maxLines: 1,
                  enableSuggestions: true,
                  autocorrect: false,
                  onChanged: (value) {},
                  style: TextStyle(
                      fontSize: 16,
                      height: 1,
                      color: Colors.black,
                      fontWeight: FontWeight.w400),
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(top: 18),
                    hintText: "Email atau Nomor Ponsel",
                    prefixIcon: Icon(Icons.person),
                    isDense: false,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20),
                child: TextField(
                    maxLines: 1,
                    obscureText: true,
                    onChanged: (value) {},
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                        fontWeight: FontWeight.w400),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(top: 18),
                      hintText: "Email atau Nomor Ponsel",
                      prefixIcon: Icon(Icons.lock),
                      suffixIcon: Icon(Icons.visibility),
                      isDense: false,
                    )),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                height: 40,
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.symmetric(horizontal: 20),
                child: FlatButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => MainScreen()));
                  },
                  child: Text(
                    "Masuk",
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  ),
                  color: clrMerahDark,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7),
                      side: BorderSide(color: Colors.red)),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Row(
                  children: [
                    Expanded(
                      child: Divider(
                        color: clrDivider,
                        height: 3,
                        thickness: 2,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 20),
                      child: Text(
                        "atau masuk dengan",
                        style: TextStyle(color: clrSubText),
                      ),
                    ),
                    Expanded(
                      child: Divider(
                        color: clrDivider,
                        height: 3,
                        thickness: 2,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                height: 40,
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: FlatButton(
                  onPressed: () {},
                  child: Row(
                    children: [
                      Image.asset("assets/img/google-logo.png"),
                      Expanded(
                        child: Center(
                          child: Text(
                            "Google",
                            style: TextStyle(color: Colors.black, fontSize: 18),
                          ),
                        ),
                      ),
                    ],
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7),
                      side: BorderSide(color: Colors.black)),
                ),
              ),
              Container(
                height: 40,
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.symmetric(
                  horizontal: 20,
                ),
                child: FlatButton(
                  onPressed: () {},
                  child: Row(
                    children: [
                      Image.asset("assets/img/facebook-logo.png"),
                      Expanded(
                        child: Center(
                          child: Text(
                            "Facebook",
                            style: TextStyle(color: Colors.black, fontSize: 18),
                          ),
                        ),
                      ),
                    ],
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7),
                      side: BorderSide(color: Colors.black)),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Belum Punya Akun? ",
                      style: TextStyle(
                        color: clrSubText,
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => RegisPage()));
                      },
                      child: Text(
                        "Daftar",
                        style: TextStyle(
                          color: clrMerahDark,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
          Container(
            margin: EdgeInsets.symmetric(vertical: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "Butuh Bantuan? ",
                  style: TextStyle(
                    color: clrSubText,
                  ),
                ),
                Text(
                  "Hubungi JualBuy Care",
                  style: TextStyle(
                    color: clrMerahDark,
                    fontWeight: FontWeight.w800,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
// =======
// import 'package:flutter/material.dart';
// import 'package:jualbay/helpers/colors.dart';
// import 'package:jualbay/page/mainscreen.dart';
// import 'package:jualbay/page/regispage.dart';
// import 'package:jualbay/widget/widget_global.dart';

// class LoginPage extends StatefulWidget {
//   const LoginPage({Key key}) : super(key: key);

//   @override
//   _LoginPageState createState() => _LoginPageState();
// }

// class _LoginPageState extends State<LoginPage> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.white,
//       resizeToAvoidBottomInset: false,
//       appBar: AppBar(
//         backgroundColor: Colors.white,
//         leading: Icon(
//           Icons.arrow_back,
//           color: Colors.black,
//         ),
//         title: Text(
//           "Masuk",
//           style: TextStyle(color: Colors.black),
//         ),
//         actions: [
//           FlatButton(
//             onPressed: () {
//               Navigator.push(context,
//                   MaterialPageRoute(builder: (context) => RegisPage()));
//             },
//             child: Text(
//               "Daftar",
//               style: TextStyle(
//                 color: clrMerahDark,
//                 fontSize: 18,
//               ),
//             ),
//           ),
//         ],
//       ),
//       body: Column(
//         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//         children: [
//           Column(
//             children: [
//               SizedBox(
//                 height: 60,
//               ),
//               Image.asset("assets/img/logo.png"),
//               SizedBox(
//                 height: 50,
//               ),
//               Container(
//                 margin: EdgeInsets.symmetric(horizontal: 20),
//                 child: TextField(
//                   maxLines: 1,
//                   enableSuggestions: true,
//                   autocorrect: false,
//                   onChanged: (value) {},
//                   style: TextStyle(
//                       fontSize: 16,
//                       height: 1,
//                       color: Colors.black,
//                       fontWeight: FontWeight.w400),
//                   keyboardType: TextInputType.emailAddress,
//                   decoration: InputDecoration(
//                     contentPadding: EdgeInsets.only(top: 18),
//                     hintText: "Email atau Nomor Ponsel",
//                     prefixIcon: Icon(Icons.person),
//                     isDense: false,
//                   ),
//                 ),
//               ),
//               Container(
//                 margin: EdgeInsets.symmetric(horizontal: 20),
//                 child: TextField(
//                     maxLines: 1,
//                     obscureText: true,
//                     onChanged: (value) {},
//                     style: TextStyle(
//                         fontSize: 16,
//                         color: Colors.black,
//                         fontWeight: FontWeight.w400),
//                     decoration: InputDecoration(
//                       contentPadding: EdgeInsets.only(top: 18),
//                       hintText: "Email atau Nomor Ponsel",
//                       prefixIcon: Icon(Icons.lock),
//                       suffixIcon: Icon(Icons.visibility),
//                       isDense: false,
//                     )),
//               ),
//               SizedBox(
//                 height: 20,
//               ),
//               PrimaryButton(
//                 title: 'Masuk',
//                 onPress: () {
//                   Navigator.push(context,
//                       MaterialPageRoute(builder: (context) => MainScreen()));
//                 },
//                 color: clrMerahDark,
//                 width: MediaQuery.of(context).size.width,
//                 marginhorizontal: 20,
//               ),
//               SizedBox(
//                 height: 10,
//               ),
//               Container(
//                 margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
//                 child: Row(
//                   children: [
//                     Expanded(
//                       child: Divider(
//                         color: clrDivider,
//                         height: 3,
//                         thickness: 2,
//                       ),
//                     ),
//                     Container(
//                       margin: EdgeInsets.symmetric(horizontal: 20),
//                       child: Text(
//                         "atau masuk dengan",
//                         style: TextStyle(color: clrSubText),
//                       ),
//                     ),
//                     Expanded(
//                       child: Divider(
//                         color: clrDivider,
//                         height: 3,
//                         thickness: 2,
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//               Container(
//                 height: 40,
//                 width: MediaQuery.of(context).size.width,
//                 margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
//                 child: FlatButton(
//                   onPressed: () {},
//                   child: Row(
//                     children: [
//                       Image.asset("assets/img/google-logo.png"),
//                       Expanded(
//                         child: Center(
//                           child: Text(
//                             "Google",
//                             style: TextStyle(color: Colors.black, fontSize: 18),
//                           ),
//                         ),
//                       ),
//                     ],
//                   ),
//                   shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(7),
//                       side: BorderSide(color: Colors.black)),
//                 ),
//               ),
//               Container(
//                 height: 40,
//                 width: MediaQuery.of(context).size.width,
//                 margin: EdgeInsets.symmetric(
//                   horizontal: 20,
//                 ),
//                 child: FlatButton(
//                   onPressed: () {},
//                   child: Row(
//                     children: [
//                       Image.asset("assets/img/facebook-logo.png"),
//                       Expanded(
//                         child: Center(
//                           child: Text(
//                             "Facebook",
//                             style: TextStyle(color: Colors.black, fontSize: 18),
//                           ),
//                         ),
//                       ),
//                     ],
//                   ),
//                   shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(7),
//                       side: BorderSide(color: Colors.black)),
//                 ),
//               ),
//               Container(
//                 margin: EdgeInsets.symmetric(vertical: 10),
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: [
//                     Text(
//                       "Belum Punya Akun? ",
//                       style: TextStyle(
//                         color: clrSubText,
//                       ),
//                     ),
//                     InkWell(
//                       onTap: () {
//                         Navigator.push(
//                             context,
//                             MaterialPageRoute(
//                                 builder: (context) => RegisPage()));
//                       },
//                       child: Text(
//                         "Daftar",
//                         style: TextStyle(
//                           color: clrMerahDark,
//                           fontWeight: FontWeight.w800,
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//               )
//             ],
//           ),
//           Container(
//             margin: EdgeInsets.symmetric(vertical: 10),
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: [
//                 Text(
//                   "Butuh Bantuan? ",
//                   style: TextStyle(
//                     color: clrSubText,
//                   ),
//                 ),
//                 Text(
//                   "Hubungi JualBuy Care",
//                   style: TextStyle(
//                     color: clrMerahDark,
//                     fontWeight: FontWeight.w800,
//                   ),
//                 ),
//               ],
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }
// >>>>>>> 625fb91f5671b4fae18b820fec00ec47d18cf23d
