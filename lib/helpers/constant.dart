import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

const stylePrice = TextStyle(
  fontWeight: FontWeight.bold,
  fontSize: 22,
  color: Colors.black,
);

const styleDiskon = TextStyle(
  fontSize: 18,
  decoration: TextDecoration.lineThrough,
  color: Colors.black,
  fontWeight: FontWeight.w800,
);

const styleTitle = TextStyle(
  fontSize: 19,
  color: Colors.black,
);
