import 'package:flutter/material.dart';

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

Color clrBasic1 = HexColor("662D91");
Color clrBasic2 = HexColor("D4145A");
Color clrBg = HexColor("F1F1F1");
Color clrMain = HexColor("FFB226");
Color clrMerahDark = HexColor("D4145A");
Color clrMerahLight = HexColor("DD437B");
Color clrUnguDark = HexColor("662D91");
Color clrUnguLight = HexColor("8557A7");
Color clrUnguGradient = HexColor("8557A7");
Color clrTitle = HexColor("000000");
Color clrGradient1 = HexColor("541C55");
Color clrGradient2 = HexColor("79267B");
Color clrDivider = HexColor("C4C4C4");
Color clrSubText = HexColor("838383");
Color clrStock = HexColor("EB5757");
Color clrbg = HexColor("F5F5F5");
Color clrIcon = HexColor("757575");
Color clrBorder = HexColor("E1E1E1");
Color clrButtonbg = HexColor("E8E8E8");
Color clrButton = HexColor("777777");
Color clrRating = HexColor("F4B719");

Color clrPgView1 = HexColor("2D9CDB");
Color clrPgView2 = HexColor("FFB226");
Color clrPgView3 = HexColor("D32267");
Color clrPgView4 = HexColor("D70000");
Color clrPgView5 = HexColor("6C6C6C");
