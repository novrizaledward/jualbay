// <<<<<<< HEAD
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:jualbay/page/component/component_homepage/costum_promo_post.dart';
import 'package:jualbay/page/loginpage.dart';
// import 'package:jualbay/page/loginpage.dart';
import 'package:provider/provider.dart';

import 'network/api_manager.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ApiManager()),
        ChangeNotifierProvider(create: (context) => UmkmManager()),
        ChangeNotifierProvider(create: (context) => SliderManager()),
        ChangeNotifierProvider(create: (context) => PromoManager()),
        ChangeNotifierProvider(create: (context) => ListManager()),
        ChangeNotifierProvider(create: (context) => IklanManager()),
        ChangeNotifierProvider(create: (context) => IklanManager()),
        ChangeNotifierProvider(create: (context) => DetailProdukProvider()),
        // ChangeNotifierProvider(create: (context) => RekomProdukKategoriProvider()),
        // ChangeNotifierProvider(create: (context) => RekomManager()),
        // ChangeNotifierProvider(create: (context) => RekomManager()),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        home: LoginPage(),
        // home: Home(),
        theme: ThemeData(textTheme: GoogleFonts.robotoTextTheme()),
      ),
    );
  }
}
// =======
// import 'package:flutter/material.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'package:jualbay/page/loginpage.dart';
// import 'package:provider/provider.dart';

// import 'network/api_manager.dart';

// void main() {
//   runApp(MyApp());
// }

// class MyApp extends StatelessWidget {
//   const MyApp({Key key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return MultiProvider(
//       providers: [
//         ChangeNotifierProvider(create: (context) => ApiManager()),
//       ],
//       child: MaterialApp(
//         debugShowCheckedModeBanner: false,
//         home: LoginPage(),
//         theme: ThemeData(textTheme: GoogleFonts.robotoTextTheme()),
//       ),
//     );
//   }
// }
// >>>>>>> 625fb91f5671b4fae18b820fec00ec47d18cf23d
