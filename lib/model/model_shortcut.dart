// To parse this JSON data, do
//
//     final modelShortcut = modelShortcutFromJson(jsonString);

import 'dart:convert';

ModelShortcut modelShortcutFromJson(String str) => ModelShortcut.fromJson(json.decode(str));

String modelShortcutToJson(ModelShortcut data) => json.encode(data.toJson());

class ModelShortcut {
    ModelShortcut({
        this.status,
        this.error,
        this.message,
        this.result,
    });

    String status;
    String error;
    String message;
    List<Result> result;

    factory ModelShortcut.fromJson(Map<String, dynamic> json) => ModelShortcut(
        status: json["status"],
        error: json["error"],
        message: json["message"],
        result: List<Result>.from(json["result"].map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "message": message,
        "result": List<dynamic>.from(result.map((x) => x.toJson())),
    };
}

class Result {
    Result({
        this.iconId,
        this.title,
        this.icon,
        this.link,
    });

    int iconId;
    String title;
    String icon;
    String link;

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        iconId: json["icon_id"],
        title: json["title"],
        icon: json["icon"],
        link: json["link"],
    );

    Map<String, dynamic> toJson() => {
        "icon_id": iconId,
        "title": title,
        "icon": icon,
        "link": link,
    };
}
