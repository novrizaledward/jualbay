// To parse this JSON data, do
//
//     final modelSlider = modelSliderFromJson(jsonString);

import 'dart:convert';

ModelSlider modelSliderFromJson(String str) => ModelSlider.fromJson(json.decode(str));

String modelSliderToJson(ModelSlider data) => json.encode(data.toJson());

class ModelSlider {
    ModelSlider({
        this.status,
        this.error,
        this.message,
        this.result,
    });

    String status;
    String error;
    String message;
    List<Result> result;

    factory ModelSlider.fromJson(Map<String, dynamic> json) => ModelSlider(
        status: json["status"],
        error: json["error"],
        message: json["message"],
        result: List<Result>.from(json["result"].map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "message": message,
        "result": List<dynamic>.from(result.map((x) => x.toJson())),
    };
}

class Result {
    Result({
        this.slidesId,
        this.buttonLink,
        this.uploadedBy,
        this.status,
        this.thumbs,
    });

    String slidesId;
    String buttonLink;
    String uploadedBy;
    String status;
    String thumbs;

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        slidesId: json["slides_id"],
        buttonLink: json["button_link"],
        uploadedBy: json["uploaded_by"],
        status: json["status"],
        thumbs: json["thumbs"],
    );

    Map<String, dynamic> toJson() => {
        "slides_id": slidesId,
        "button_link": buttonLink,
        "uploaded_by": uploadedBy,
        "status": status,
        "thumbs": thumbs,
    };
}
