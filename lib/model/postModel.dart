class PostModel {
  String status;
  String error;
  String message;
  List<Result> result;

  PostModel({this.status, this.error, this.message, this.result});

  PostModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    error = json['error'];
    message = json['message'];
    if (json['result'] != null) {
      result = new List<Result>();
      json['result'].forEach((v) {
        result.add(new Result.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['error'] = this.error;
    data['message'] = this.message;
    if (this.result != null) {
      data['result'] = this.result.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Result {
  int productId;
  String thumbs;
  String title;
  String displayName;
  int salePrice;
  bool discountType;
  bool discount;
  int finalPrice;
  bool cod;
  bool freeship;

  Result(
      {this.productId,
      this.thumbs,
      this.title,
      this.displayName,
      this.salePrice,
      this.discountType,
      this.discount,
      this.finalPrice,
      this.cod,
      this.freeship});

  Result.fromJson(Map<String, dynamic> json) {
    productId = json['product_id'];
    thumbs = json['thumbs'];
    title = json['title'];
    displayName = json['display_name'];
    salePrice = json['sale_price'];
    discountType = json['discount_type'];
    discount = json['discount'];
    finalPrice = json['final_price'];
    cod = json['cod'];
    freeship = json['freeship'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['product_id'] = this.productId;
    data['thumbs'] = this.thumbs;
    data['title'] = this.title;
    data['display_name'] = this.displayName;
    data['sale_price'] = this.salePrice;
    data['discount_type'] = this.discountType;
    data['discount'] = this.discount;
    data['final_price'] = this.finalPrice;
    data['cod'] = this.cod;
    data['freeship'] = this.freeship;
    return data;
  }
}
