// To parse this JSON data, do
//
//     final modelPromo = modelPromoFromJson(jsonString);

import 'dart:convert';

ModelPromo modelPromoFromJson(String str) => ModelPromo.fromJson(json.decode(str));

String modelPromoToJson(ModelPromo data) => json.encode(data.toJson());

class ModelPromo {
    ModelPromo({
        this.status,
        this.error,
        this.message,
        this.result,
    });

    String status;
    String error;
    String message;
    List<Result> result;

    factory ModelPromo.fromJson(Map<String, dynamic> json) => ModelPromo(
        status: json["status"],
        error: json["error"],
        message: json["message"],
        result: List<Result>.from(json["result"].map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "message": message,
        "result": List<dynamic>.from(result.map((x) => x.toJson())),
    };
}

class Result {
    Result({
        this.productId,
        this.thumbs,
        this.title,
        this.displayName,
        this.salePrice,
        this.discountType,
        this.discount,
        this.finalPrice,
        this.cod,
        this.freeship,
    });

    int productId;
    String thumbs;
    String title;
    DisplayName displayName;
    int salePrice;
    dynamic discountType;
    dynamic discount;
    int finalPrice;
    bool cod;
    bool freeship;

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        productId: json["product_id"],
        thumbs: json["thumbs"],
        title: json["title"],
        displayName: displayNameValues.map[json["display_name"]],
        salePrice: json["sale_price"],
        discountType: json["discount_type"],
        discount: json["discount"],
        finalPrice: json["final_price"],
        cod: json["cod"],
        freeship: json["freeship"],
    );

    Map<String, dynamic> toJson() => {
        "product_id": productId,
        "thumbs": thumbs,
        "title": title,
        "display_name": displayNameValues.reverse[displayName],
        "sale_price": salePrice,
        "discount_type": discountType,
        "discount": discount,
        "final_price": finalPrice,
        "cod": cod,
        "freeship": freeship,
    };
}

enum DiscountTypeEnum { PERCENT, AMOUNT }

final discountTypeEnumValues = EnumValues({
    "amount": DiscountTypeEnum.AMOUNT,
    "percent": DiscountTypeEnum.PERCENT
});

enum DisplayName { YOYO_SHOP_PKU, WIECRAFT, BISMILLAH }

final displayNameValues = EnumValues({
    "Bismillah": DisplayName.BISMILLAH,
    "Wiecraft": DisplayName.WIECRAFT,
    "YOYO_SHOP PKU": DisplayName.YOYO_SHOP_PKU
});

class EnumValues<T> {
    Map<String, T> map;
    Map<T, String> reverseMap;

    EnumValues(this.map);

    Map<T, String> get reverse {
        if (reverseMap == null) {
            reverseMap = map.map((k, v) => new MapEntry(v, k));
        }
        return reverseMap;
    }
}
