// To parse this JSON data, do
//
//     final modelRekomProduk = modelRekomProdukFromJson(jsonString);

import 'dart:convert';

ModelRekomProduk modelRekomProdukFromJson(String str) => ModelRekomProduk.fromJson(json.decode(str));

String modelRekomProdukToJson(ModelRekomProduk data) => json.encode(data.toJson());

class ModelRekomProduk {
    ModelRekomProduk({
        this.status,
        this.error,
        this.message,
        this.result,
    });

    String status;
    String error;
    String message;
    List<Result> result;

    factory ModelRekomProduk.fromJson(Map<String, dynamic> json) => ModelRekomProduk(
        status: json["status"],
        error: json["error"],
        message: json["message"],
        result: List<Result>.from(json["result"].map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "message": message,
        "result": List<dynamic>.from(result.map((x) => x.toJson())),
    };
}

class Result {
    Result({
        this.productId,
        this.thumbs,
        this.title,
        this.displayName,
        this.salePrice,
        this.discountType,
        this.discount,
        this.finalPrice,
        this.cod,
        this.freeship,
        this.currentStock,
    });

    int productId;
    String thumbs;
    String title;
    String displayName;
    int salePrice;
    dynamic discountType;
    dynamic discount;
    int finalPrice;
    bool cod;
    bool freeship;
    int currentStock;

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        productId: json["product_id"],
        thumbs: json["thumbs"],
        title: json["title"],
        displayName: json["display_name"],
        salePrice: json["sale_price"],
        discountType: json["discount_type"],
        discount: json["discount"],
        finalPrice: json["final_price"],
        cod: json["cod"],
        freeship: json["freeship"],
        currentStock: json["current_stock"],
    );

    Map<String, dynamic> toJson() => {
        "product_id": productId,
        "thumbs": thumbs,
        "title": title,
        "display_name": displayName,
        "sale_price": salePrice,
        "discount_type": discountType,
        "discount": discount,
        "final_price": finalPrice,
        "cod": cod,
        "freeship": freeship,
        "current_stock": currentStock,
    };
}
