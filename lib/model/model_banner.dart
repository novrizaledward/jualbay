// To parse this JSON data, do
//
//     final modelBanner = modelBannerFromJson(jsonString);

import 'dart:convert';

ModelBanner modelBannerFromJson(String str) => ModelBanner.fromJson(json.decode(str));

String modelBannerToJson(ModelBanner data) => json.encode(data.toJson());

class ModelBanner {
    ModelBanner({
        this.status,
        this.error,
        this.message,
        this.result,
    });

    String status;
    String error;
    String message;
    Result result;

    factory ModelBanner.fromJson(Map<String, dynamic> json) => ModelBanner(
        status: json["status"],
        error: json["error"],
        message: json["message"],
        result: Result.fromJson(json["result"]),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "message": message,
        "result": result.toJson(),
    };
}

class Result {
    Result({
        this.h1,
        this.h2,
        this.h3,
        this.ukm,
        this.h4,
        this.h5,
    });

    H1 h1;
    H1 h2;
    H1 h3;
    H1 ukm;
    H1 h4;
    H1 h5;

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        h1: H1.fromJson(json["h1"]),
        h2: H1.fromJson(json["h2"]),
        h3: H1.fromJson(json["h3"]),
        ukm: H1.fromJson(json["ukm"]),
        h4: H1.fromJson(json["h4"]),
        h5: H1.fromJson(json["h5"]),
    );

    Map<String, dynamic> toJson() => {
        "h1": h1.toJson(),
        "h2": h2.toJson(),
        "h3": h3.toJson(),
        "ukm": ukm.toJson(),
        "h4": h4.toJson(),
        "h5": h5.toJson(),
    };
}

class H1 {
    H1({
        this.id,
        this.alt,
        this.banner,
        this.link,
    });

    String id;
    String alt;
    String banner;
    String link;

    factory H1.fromJson(Map<String, dynamic> json) => H1(
        id: json["id"],
        alt: json["alt"],
        banner: json["banner"],
        link: json["link"] == null ? null : json["link"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "alt": alt,
        "banner": banner,
        "link": link == null ? null : link,
    };
}
