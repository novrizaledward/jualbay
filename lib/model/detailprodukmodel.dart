// To parse this JSON data, do
//
//     final detailProdukModel = detailProdukModelFromJson(jsonString);

import 'dart:convert';

DetailProdukModel detailProdukModelFromJson(String str) => DetailProdukModel.fromJson(json.decode(str));

String detailProdukModelToJson(DetailProdukModel data) => json.encode(data.toJson());

class DetailProdukModel {
    DetailProdukModel({
        this.status,
        this.error,
        this.message,
        this.reslut,
    });

    String status;
    String error;
    String message;
    List<Reslut> reslut;

    factory DetailProdukModel.fromJson(Map<String, dynamic> json) => DetailProdukModel(
        status: json["status"],
        error: json["error"],
        message: json["message"],
        reslut: List<Reslut>.from(json["reslut"].map((x) => Reslut.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "message": message,
        "reslut": List<dynamic>.from(reslut.map((x) => x.toJson())),
    };
}

class Reslut {
    Reslut({
        this.categoryId,
        this.categoryName,
        this.banner,
        this.productId,
        this.title,
        this.salePrice,
        this.discountType,
        this.discount,
        this.description,
        this.productCondition,
        this.weight,
        this.purchaseMin,
        this.addTimestamp,
        this.vendorId,
        this.displayName,
        this.city,
        this.lastLogin,
        this.thumbsSlide,
        this.finalPrice,
        this.discountStatus,
        this.thumbs,
        this.icons,
        this.codStatus,
        this.freeshipStatus,
        this.imageVendor,
        // this.variant,
        this.voucher,
        this.comment,
        this.pilihanToko,
        this.rekom,
    });

    String categoryId;
    String categoryName;
    String banner;
    int productId;
    String title;
    String salePrice;
    dynamic discountType;
    String discount;
    String description;
    String productCondition;
    String weight;
    String purchaseMin;
    String addTimestamp;
    String vendorId;
    String displayName;
    String city;
    String lastLogin;
    List<String> thumbsSlide;
    int finalPrice;
    dynamic discountStatus;
    String thumbs;
    List<String> icons;
    dynamic codStatus;
    dynamic freeshipStatus;
    String imageVendor;
    // Variant variant;
    List<Voucher> voucher;
    List<Comment> comment;
    List<PilihanToko> pilihanToko;
    List<Rekom> rekom;

    factory Reslut.fromJson(Map<String, dynamic> json) => Reslut(
        categoryId: json["category_id"],
        categoryName: json["category_name"],
        banner: json["banner"],
        productId: json["product_id"],
        title: json["title"],
        salePrice: json["sale_price"],
        discountType: json["discount_type"],
        discount: json["discount"],
        description: json["description"],
        productCondition: json["product_condition"],
        weight: json["weight"],
        purchaseMin: json["purchase_min"],
        addTimestamp: json["add_timestamp"],
        vendorId: json["vendor_id"],
        displayName: json["display_name"],
        city: json["city"],
        lastLogin: json["last_login"],
        thumbsSlide: List<String>.from(json["thumbs_slide"].map((x) => x)),
        finalPrice: json["final_price"],
        discountStatus: json["discount_status"],
        thumbs: json["thumbs"],
        icons: List<String>.from(json["icons"].map((x) => x)),
        codStatus: json["cod_status"],
        freeshipStatus: json["Freeship_status"],
        imageVendor: json["image_vendor"],
        // variant: Variant.fromJson(json["variant"]),
        voucher: List<Voucher>.from(json["voucher"].map((x) => Voucher.fromJson(x))),
        comment: List<Comment>.from(json["comment"].map((x) => Comment.fromJson(x))),
        pilihanToko: List<PilihanToko>.from(json["pilihan_toko"].map((x) => PilihanToko.fromJson(x))),
        rekom: List<Rekom>.from(json["rekom"].map((x) => Rekom.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "category_id": categoryId,
        "category_name": categoryName,
        "banner": banner,
        "product_id": productId,
        "title": title,
        "sale_price": salePrice,
        "discount_type": discountType,
        "discount": discount,
        "description": description,
        "product_condition": productCondition,
        "weight": weight,
        "purchase_min": purchaseMin,
        "add_timestamp": addTimestamp,
        "vendor_id": vendorId,
        "display_name": displayName,
        "city": city,
        "last_login": lastLogin,
        "thumbs_slide": List<dynamic>.from(thumbsSlide.map((x) => x)),
        "final_price": finalPrice,
        "discount_status": discountStatus,
        "thumbs": thumbs,
        "icons": List<dynamic>.from(icons.map((x) => x)),
        "cod_status": codStatus,
        "Freeship_status": freeshipStatus,
        "image_vendor": imageVendor,
        // "variant": variant.toJson(),
        "voucher": List<dynamic>.from(voucher.map((x) => x.toJson())),
        "comment": List<dynamic>.from(comment.map((x) => x.toJson())),
        "pilihan_toko": List<dynamic>.from(pilihanToko.map((x) => x.toJson())),
        "rekom": List<dynamic>.from(rekom.map((x) => x.toJson())),
    };
}

class Comment {
    Comment({
        this.komenId,
        this.userId,
        this.userStatus,
        this.productId,
        this.komenStatus,
        this.komenIsi,
        this.tanggal,
        this.username,
    });

    String komenId;
    String userId;
    String userStatus;
    String productId;
    String komenStatus;
    String komenIsi;
    DateTime tanggal;
    String username;

    factory Comment.fromJson(Map<String, dynamic> json) => Comment(
        komenId: json["komen_id"],
        userId: json["user_id"],
        userStatus: json["user_status"],
        productId: json["product_id"],
        komenStatus: json["komen_status"],
        komenIsi: json["komen_isi"],
        tanggal: DateTime.parse(json["tanggal"]),
        username: json["username"],
    );

    Map<String, dynamic> toJson() => {
        "komen_id": komenId,
        "user_id": userId,
        "user_status": userStatus,
        "product_id": productId,
        "komen_status": komenStatus,
        "komen_isi": komenIsi,
        "tanggal": tanggal.toIso8601String(),
        "username": username,
    };
}

class PilihanToko {
    PilihanToko({
        this.productId,
        this.salePrice,
        this.title,
        this.finalPrice,
        this.discount,
        this.discountStatus,
        this.discountType,
        this.codStatus,
        this.freeshipStatus,
        this.thumbs,
    });

    String productId;
    String salePrice;
    String title;
    int finalPrice;
    String discount;
    dynamic discountStatus;
    dynamic discountType;
    dynamic codStatus;
    dynamic freeshipStatus;
    String thumbs;

    factory PilihanToko.fromJson(Map<String, dynamic> json) => PilihanToko(
        productId: json["product_id"],
        salePrice: json["sale_price"],
        title: json["title"],
        finalPrice: json["final_price"],
        discount: json["discount"],
        discountStatus: json["discount_status"],
        discountType: json["discount_type"],
        codStatus: json["cod_status"],
        freeshipStatus: json["Freeship_status"],
        thumbs: json["thumbs"],
    );

    Map<String, dynamic> toJson() => {
        "product_id": productId,
        "sale_price": salePrice,
        "title": title,
        "final_price": finalPrice,
        "discount": discount,
        "discount_status": discountStatus,
        "discount_type": discountType,
        "cod_status": codStatus,
        "Freeship_status": freeshipStatus,
        "thumbs": thumbs,
    };
}

class Rekom {
    Rekom({
        this.categoryId,
        this.productId,
        this.thumbs,
        this.title,
        this.displayName,
        this.salePrice,
        this.discountType,
        this.discountStatus,
        this.discount,
        this.finalPrice,
        this.cod,
        this.freeship,
        this.currentStock,
    });

    int categoryId;
    int productId;
    String thumbs;
    String title;
    String displayName;
    int salePrice;
    dynamic discountType;
    dynamic discountStatus;
    dynamic discount;
    int finalPrice;
    dynamic cod;
    dynamic freeship;
    int currentStock;

    factory Rekom.fromJson(Map<String, dynamic> json) => Rekom(
        categoryId: json["category_id"],
        productId: json["product_id"],
        thumbs: json["thumbs"],
        title: json["title"],
        displayName: json["display_name"],
        salePrice: json["sale_price"],
        discountType: json["discount_type"],
        discountStatus: json["discount_status"],
        discount: json["discount"],
        finalPrice: json["final_price"],
        cod: json["cod"],
        freeship: json["freeship"],
        currentStock: json["current_stock"],
    );

    Map<String, dynamic> toJson() => {
        "category_id": categoryId,
        "product_id": productId,
        "thumbs": thumbs,
        "title": title,
        "display_name": displayName,
        "sale_price": salePrice,
        "discount_type": discountType,
        "discount_status": discountStatus,
        "discount": discount,
        "final_price": finalPrice,
        "cod": cod,
        "freeship": freeship,
        "current_stock": currentStock,
    };
}

class Variant {
    Variant({
        this.hijau,
        this.merah,
        this.hitam,
    });

    Hijau hijau;
    Hijau merah;
    Hitam hitam;

    factory Variant.fromJson(Map<String, dynamic> json) => Variant(
        hijau: Hijau.fromJson(json["Hijau"]),
        merah: Hijau.fromJson(json["Merah"]),
        hitam: Hitam.fromJson(json["Hitam"]),
    );

    Map<String, dynamic> toJson() => {
        "Hijau": hijau.toJson(),
        "Merah": merah.toJson(),
        "Hitam": hitam.toJson(),
    };
}

class Hijau {
    Hijau({
        this.l,
    });

    L l;

    factory Hijau.fromJson(Map<String, dynamic> json) => Hijau(
        l: L.fromJson(json["L"]),
    );

    Map<String, dynamic> toJson() => {
        "L": l.toJson(),
    };
}

class L {
    L({
        this.productId,
        this.price,
        this.stock,
        this.varImages,
    });

    String productId;
    String price;
    String stock;
    String varImages;

    factory L.fromJson(Map<String, dynamic> json) => L(
        productId: json["product_id"],
        price: json["price"],
        stock: json["stock"],
        varImages: json["var_images"],
    );

    Map<String, dynamic> toJson() => {
        "product_id": productId,
        "price": price,
        "stock": stock,
        "var_images": varImages,
    };
}

class Hitam {
    Hitam({
        this.xxl,
    });

    L xxl;

    factory Hitam.fromJson(Map<String, dynamic> json) => Hitam(
        xxl: L.fromJson(json["XXL"]),
    );

    Map<String, dynamic> toJson() => {
        "XXL": xxl.toJson(),
    };
}

class Voucher {
    Voucher({
        this.voucherId,
        this.code,
        this.title,
        this.value,
        this.datestringStart,
        this.datestringEnd,
        this.status,
        this.datetimeEnd,
        this.datetimeStart,
    });

    String voucherId;
    String code;
    String title;
    String value;
    String datestringStart;
    String datestringEnd;
    String status;
    DateTime datetimeEnd;
    DateTime datetimeStart;

    factory Voucher.fromJson(Map<String, dynamic> json) => Voucher(
        voucherId: json["voucher_id"],
        code: json["code"],
        title: json["title"],
        value: json["value"],
        datestringStart: json["datestring_start"],
        datestringEnd: json["datestring_end"],
        status: json["status"],
        datetimeEnd: DateTime.parse(json["datetime_end"]),
        datetimeStart: DateTime.parse(json["datetime_start"]),
    );

    Map<String, dynamic> toJson() => {
        "voucher_id": voucherId,
        "code": code,
        "title": title,
        "value": value,
        "datestring_start": datestringStart,
        "datestring_end": datestringEnd,
        "status": status,
        "datetime_end": datetimeEnd.toIso8601String(),
        "datetime_start": datetimeStart.toIso8601String(),
    };
}
