// To parse this JSON data, do
//
//     final modelProductView = modelProductViewFromJson(jsonString);

import 'dart:convert';

ModelProductView modelProductViewFromJson(String str) => ModelProductView.fromJson(json.decode(str));

String modelProductViewToJson(ModelProductView data) => json.encode(data.toJson());

class ModelProductView {
    ModelProductView({
        this.status,
        this.error,
        this.message,
        this.reslut,
    });

    String status;
    String error;
    String message;
    List<Reslut> reslut;

    factory ModelProductView.fromJson(Map<String, dynamic> json) => ModelProductView(
        status: json["status"],
        error: json["error"],
        message: json["message"],
        reslut: List<Reslut>.from(json["reslut"].map((x) => Reslut.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "message": message,
        "reslut": List<dynamic>.from(reslut.map((x) => x.toJson())),
    };
}

class Reslut {
    Reslut({
        this.categoryId,
        this.categoryName,
        this.banner,
        this.productId,
        this.title,
        this.salePrice,
        this.discountType,
        this.discount,
        this.description,
        this.productCondition,
        this.weight,
        this.purchaseMin,
        this.vendorId,
        this.displayName,
        this.city,
        this.lastLogin,
        this.thumbsSlide,
        this.finalPrice,
        this.thumbs,
        this.imageVendor,
        this.variant,
    });

    String categoryId;
    String categoryName;
    String banner;
    int productId;
    String title;
    String salePrice;
    String discountType;
    String discount;
    String description;
    String productCondition;
    String weight;
    String purchaseMin;
    String vendorId;
    String displayName;
    String city;
    String lastLogin;
    List<String> thumbsSlide;
    int finalPrice;
    String thumbs;
    String imageVendor;
    Variant variant;

    factory Reslut.fromJson(Map<String, dynamic> json) => Reslut(
        categoryId: json["category_id"],
        categoryName: json["category_name"],
        banner: json["banner"],
        productId: json["product_id"],
        title: json["title"],
        salePrice: json["sale_price"],
        discountType: json["discount_type"],
        discount: json["discount"],
        description: json["description"],
        productCondition: json["product_condition"],
        weight: json["weight"],
        purchaseMin: json["purchase_min"],
        vendorId: json["vendor_id"],
        displayName: json["display_name"],
        city: json["city"],
        lastLogin: json["last_login"],
        thumbsSlide: List<String>.from(json["thumbs_slide"].map((x) => x)),
        finalPrice: json["final_price"],
        thumbs: json["thumbs"],
        imageVendor: json["image_vendor"],
        variant: Variant.fromJson(json["variant"]),
    );

    Map<String, dynamic> toJson() => {
        "category_id": categoryId,
        "category_name": categoryName,
        "banner": banner,
        "product_id": productId,
        "title": title,
        "sale_price": salePrice,
        "discount_type": discountType,
        "discount": discount,
        "description": description,
        "product_condition": productCondition,
        "weight": weight,
        "purchase_min": purchaseMin,
        "vendor_id": vendorId,
        "display_name": displayName,
        "city": city,
        "last_login": lastLogin,
        "thumbs_slide": List<dynamic>.from(thumbsSlide.map((x) => x)),
        "final_price": finalPrice,
        "thumbs": thumbs,
        "image_vendor": imageVendor,
        "variant": variant.toJson(),
    };
}

class Variant {
    Variant({
        this.madu,
    });

    Madu madu;

    factory Variant.fromJson(Map<String, dynamic> json) => Variant(
        madu: Madu.fromJson(json["Madu"]),
    );

    Map<String, dynamic> toJson() => {
        "Madu": madu.toJson(),
    };
}

class Madu {
    Madu({
        this.the250,
    });

    The250 the250;

    factory Madu.fromJson(Map<String, dynamic> json) => Madu(
        the250: The250.fromJson(json["250"]),
    );

    Map<String, dynamic> toJson() => {
        "250": the250.toJson(),
    };
}

class The250 {
    The250({
        this.productId,
        this.price,
        this.stock,
        this.varImages,
    });

    String productId;
    String price;
    String stock;
    String varImages;

    factory The250.fromJson(Map<String, dynamic> json) => The250(
        productId: json["product_id"],
        price: json["price"],
        stock: json["stock"],
        varImages: json["var_images"],
    );

    Map<String, dynamic> toJson() => {
        "product_id": productId,
        "price": price,
        "stock": stock,
        "var_images": varImages,
    };
}
