// To parse this JSON data, do
//
//     final modelRekomKategori = modelRekomKategoriFromJson(jsonString);

import 'dart:convert';
 

ModelRekomKategori modelRekomKategoriFromJson(String str) => ModelRekomKategori.fromJson(json.decode(str));

String modelRekomKategoriToJson(ModelRekomKategori data) => json.encode(data.toJson());


class ModelRekomKategori {
    ModelRekomKategori({
        this.categoryId,
        this.productId,
        this.thumbs,
        this.title,
        this.displayName,
        this.salePrice,
        this.discountType,
        this.discountStatus,
        this.discount,
        this.finalPrice,
        this.cod,
        this.freeship,
        this.currentStock,
    });

    int categoryId;
    int productId;
    String thumbs;
    String title;
    String displayName;
    int salePrice;
    dynamic discountType;
    bool discountStatus;
    dynamic discount;
    int finalPrice;
    bool cod;
    bool freeship;
    int currentStock;

    factory ModelRekomKategori.fromJson(Map<String, dynamic> json) => ModelRekomKategori(
        categoryId: json["category_id"],
        productId: json["product_id"],
        thumbs: json["thumbs"],
        title: json["title"],
        displayName: json["display_name"],
        salePrice: json["sale_price"],
        discountType: json["discount_type"],
        discountStatus: json["discount_status"],
        discount: json["discount"],
        finalPrice: json["final_price"],
        cod: json["cod"],
        freeship: json["freeship"],
        currentStock: json["current_stock"],
    );

    Map<String, dynamic> toJson() => {
        "category_id": categoryId,
        "product_id": productId,
        "thumbs": thumbs,
        "title": title,
        "display_name": displayName,
        "sale_price": salePrice,
        "discount_type": discountType,
        "discount_status": discountStatus,
        "discount": discount,
        "final_price": finalPrice,
        "cod": cod,
        "freeship": freeship,
        "current_stock": currentStock,
    };
}
